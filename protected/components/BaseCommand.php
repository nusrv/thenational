<?php
class BaseCommand extends CConsoleCommand
{


    private $details;
    private $category;
    private $model;
    private $trim_hash;
    private $hash_tag;
    private $platform;
    private $platform_id;
    private $parent = null;
    private $creator;
    private $sub_category;
    private $source;
    private $news;
    private $PostQueue;
    private $post_id;
    private $general_image= null;

    private $image_copyright = array(' AP ',' AFP ',' Reuters ' , ' Getty Images ' , ' EPA ' , ' Action Images ',' WAM ');
    private $debug = false;
    private $image_instagram_scheduled = false;
    public function bitShort($url){
        $d = Yii::app()->bitly->shorten($url)->getResponseData();
        $d = CJSON::decode($d);
        if(isset($d['status_code']) && $d['status_code'] == 200){
            if(isset($d['data'])){
                $data = $d['data'];
                if(isset($data['url'])){
                    return $data['url'];
                }
            }
        }

        return null;
    }
    function file_get_contents_curl($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
        curl_setopt($ch, CURLOPT_URL, $url);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    public function TimeZone()
    {
        $data = Settings::model()->find('category_id is NULL');
        Yii::app()->timeZone = 'Asia/Dubai';
        if(!empty($data))
            Yii::app()->timeZone = empty($data->timezone)?'Asia/Dubai':$data->timezone;
    }

    public function upload_pdf($file){
        $file = Yii::app()->params['webroot'].$file;
        $s3 = new S3(Yii::app()->params['amazonAccessKey'], Yii::app()->params['amazonSecretKey']);

        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $s3->putBucket(Yii::app()->params['amazonBucket'], S3::ACL_PUBLIC_READ);
        $filename='pdf/pdf_'.time().'.'.$ext;
        if ($s3->putObjectFile($file, Yii::app()->params['amazonBucket'], Yii::app()->params['amazonFolder'].'/'.$filename , S3::ACL_PUBLIC_READ))
            return  'http://s3.amazonaws.com/'.Yii::app()->params['amazonBucket'].'/'.Yii::app()->params['amazonFolder'].'/'.$filename;

        return null;
    }

    protected function Load(){

        $FACEBOOK_APP_ID = Yii::app()->params['facebook']['app_id']; // Your facebook app ID
        $FACEBOOK_SECRET = Yii::app()->params['facebook']['secret']; // Your facebook secret
        $ACCESS_TOKEN = Yii::app()->params['facebook']['token']; // The access token you receieved above
        $facebook = new Facebook\Facebook([
            'app_id' => $FACEBOOK_APP_ID,
            'app_secret' => $FACEBOOK_SECRET,
            'default_graph_version' => 'v2.5',
        ]);

        $PAGE_TOKEN = null;
        try {
            $res = $facebook->get('/me/accounts', $ACCESS_TOKEN);

            if(json_decode($res->getBody())->data){
                foreach (json_decode($res->getBody())->data as $item) {
                    if (Yii::app()->params['facebook']['page_id'] == $item->id) {
                        $PAGE_TOKEN = $item->access_token;
                        break;
                    }
                }
            }

            if($PAGE_TOKEN == null){

                $PAGE_TOKEN =  $this->fb_next_page_access($res,$facebook);

            }


        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return array($facebook, $PAGE_TOKEN);
    }

    public function fb_next_page_access($res , $facebook){

        if($res->getGraphEdge()->getNextCursor())
            $r =   $facebook->next($res->getGraphEdge());


        if($r->asArray()){
            foreach ($r->asArray() as $item) {
                if (Yii::app()->params['facebook']['page_id'] == $item['id']) {
                    return $item['access_token'];
                }
            }
        }

        $this->fb_next_page_access($r,$facebook);

    }


    function instagram_image($photo, $caption,$auth){
        $i = new Instagram($auth['username'], $auth['password'], $auth['debug']);
        try {
            $i->login();
        } catch (InstagramException $e) {
            echo $e->getMessage();
            return false;
        }
        try {
           return $i->uploadPhoto($photo, $caption);
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }

    }

    public function Obj_twitter(){
        $auth = Yii::app()->params['twitter'];
        Codebird::setConsumerKey($auth['key'], $auth['secret']);
        $cb = Codebird::getInstance();
        $cb->setToken($auth['token'], $auth['token_secret']);
        return $cb;
    }

    public function Size_video($file){


        $getID3 = new getID3;

        $file = $getID3->analyze($file);

        if(isset($file['filesize']) && isset($file['playtime_string'])){

            if($file['filesize']<536870912){

                $time = explode(':',$file['playtime_string']);

                if(isset($time[0]) && isset($time[1])){

                    $time[0] = $time[0] * 60;

                    $time = $time[0] + $time[1];

                    if($time<=140){

                        return true;

                    }
                }

            }
        }

        return false;
    }


    public function upload_file($file){
        Yii::import('application.components.S3');
        if(!empty($file)){
            $s3 = new S3(Yii::app()->params['amazonAccessKey'], Yii::app()->params['amazonSecretKey']);
            $path = $file;
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $s3->putBucket(Yii::app()->params['amazonBucket'], S3::ACL_PUBLIC_READ);
            $filename='cron_'.time().'.'.$ext;
            $fileUrl='';
            if ($s3->putObjectFile($file, Yii::app()->params['amazonBucket'], Yii::app()->params['amazonFolder'].'/'.$filename , S3::ACL_PUBLIC_READ)) {
                $fileUrl = 'http://s3.amazonaws.com/'.Yii::app()->params['amazonBucket'].'/'.Yii::app()->params['amazonFolder'].'/'.$filename;
            }
            unlink($file);
            return array(true,$fileUrl);
        }else{
            return array(false,'');
        }
    }
    public function upload_file_video($file,$deleted = true){
        Yii::import('application.components.S3');
        if(!empty($file)){
            $s3 = new S3(Yii::app()->params['amazonAccessKey'], Yii::app()->params['amazonSecretKey']);
            $path = $file;
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $s3->putBucket(Yii::app()->params['amazonBucket'], S3::ACL_PUBLIC_READ);
            $filename='cron_'.time().'.'.$ext;
            $fileUrl='';
            if ($s3->putObjectFile($file, Yii::app()->params['amazonBucket'], Yii::app()->params['amazonFolder'].'/'.$filename , S3::ACL_PUBLIC_READ)) {
                $fileUrl = 'http://s3.amazonaws.com/'.Yii::app()->params['amazonBucket'].'/'.Yii::app()->params['amazonFolder'].'/'.$filename;
            }


            return $fileUrl;
        }else{
            return array(false,'');
        }
    }
    private function get_platform_update($platform = 'Twitter'){

        $id = null;
        $PlatFrom = Platform::model()->findAllByAttributes(array('deleted' => 0));

        if(!empty(array_search($platform, $PlatFrom))){

            $id = $PlatFrom[array_search($platform, $PlatFrom)]->id;

        }else{
            $id = Platform::model()->findByAttributes(array('title'=>$platform));

            $id = !empty($id)?$id->id:null;

        }

        return $id;
    }
    public function get_video_in_news($id){



        $data = !empty(MediaNews::model()->findByAttributes(array('news_id'=>$id,'type'=>'video')))?true:false;

        if($this->debug)
            echo '[generate] : now in get Video in news is '.$data. PHP_EOL;

        return $data ;
    }
    public function Video_downloader($url){



        if($this->debug)
            echo '[generate] : now in downloader video  in news ' . PHP_EOL;

        $rx = '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i';

        $path = Yii::app()->params['webroot'] . '/video/video_' . time() . '.mp4';
        if($this->debug)
            echo '[generate] : now in downloader video preg_match  in news ' . PHP_EOL;

        $download_video_url = "'$url'";

        exec("/usr/local/bin/youtube-dl -F $download_video_url", $out);

        if (!empty($out)) {

            if($this->debug)
                echo '[generate] : now in downloader video if (!empty($out))   in news ' . PHP_EOL;

            if (isset(explode(' ', $out[count($out) - 1])[0])) {

                $id = explode(' ', $out[count($out) - 1])[0];

                if(exec("/usr/local/bin/youtube-dl -o '$path' $download_video_url -f '$id'", $output)) {
                    end($output);

                    if(strpos(end($output), '100%') !== false || strpos(prev($output), '100%') !== false){
                        return array(true, $path);
                    }
                }
            }
        }
        if (is_dir($path))
            return array(false,false);

        return array(false,false);
    }
    public function Video_size_check_length($file){


        $getID3 = new getID3;

        $file = $getID3->analyze($file);

        if(isset($file['filesize']) && isset($file['playtime_string'])){



            $time = explode(':',$file['playtime_string']);

            if(isset($time[0]) && isset($time[1])){

                $time[0] = $time[0] * 60;

                $time = $time[0] + $time[1];

                if($time<=1800){

                    if($this->debug)
                        echo '[Generator] : Video time ='.$time.PHP_EOL;

                    return true;

                }else{
                    return false;
                }
            }
        }

        return false;
    }
    public function get_media_news($news){


        $media_array = MediaNews::model()->findByAttributes(array('news_id'=>$news->id,'type'=>'video'));



        if(!empty($media_array->media)){

            /*Start Youtube*/

            if($rx = '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i');


            if ($this->debug)
                echo '[generate] : now in preg_match is  : true in news ' . PHP_EOL;

            list($valid, $path) = $this->Video_downloader($media_array->media);

            if ($this->Video_size_check_length($path)) {

                if ($valid) {
                    $media_array->media = $this->upload_file_video($path,false);



                    if ($media_array->save()) {

                        return array($media_array, true, $path);

                    }
                }
            }else{

                if($this->debug)
                echo '[generate] : Video is longer than half an hour' . PHP_EOL;

                return array(false,false,false);
            }


            /*End Youtube*/
        }


        return array(false,false,false);

    }
    public function video_size($file){

        Yii::import('application.extensions.getid3.getID3');

        $getID3 = new getID3;

        $file = $getID3->analyze($file);

        if(isset($file['filesize']) && isset($file['playtime_string'])){

            if($file['filesize']<536870912){

                if($this->debug)
                echo $file['filesize'].PHP_EOL;
                $time = explode(':',$file['playtime_string']);

                if(isset($time[0]) && isset($time[1])){

                    $time[0] = $time[0] * 60;

                    $time = $time[0] + $time[1];
                    if($this->debug)
                    echo $time.PHP_EOL;
                    if($time<=140){

                        return true;

                    }
                }

            }
        }

        return false;
    }
    public function delete_file_from_server($file){

        if($this->debug)
            echo '[generate] : now in delete_file_from_server : -> '.$file.'  in news ' . PHP_EOL;

        if(file_exists($file))
        unlink($file);
    }
    public function get_media_for_twitter($news){
        $cdb = new CDbCriteria();

        $cdb->condition = 'news_id = '.$news->id.' and type != "video"';

        return  MediaNews::model()->find($cdb);
    }

    public function get_media_for_instagram($news){

        $cdb = new CDbCriteria();

        $cdb->condition = 'news_id = '.$news->id.' and type != "video" and type != "gallery"';

        return  MediaNews::model()->find($cdb);
    }
    public function get_template($platform, $category,$type = null){

        $type = ucfirst($type);

        if($this->debug)
            echo '[Generator] : Type get Template '.$type.PHP_EOL;

        if($type == 'Video') {

            $temp = PostTemplate::model()->findByAttributes(array(
                'platform_id' => $platform->id,
                'catgory_id' => $category,
                'type' => $type,
            ));

            if (!empty($temp)) {

                if($this->debug)
                    echo '[generate] : \'platform_id\' => $platform->id,
            \'catgory_id\' => $category,
            \'type\' => $type,' . PHP_EOL . PHP_EOL . PHP_EOL;

                return $temp;
            }

            $cond = new CDbCriteria();
            $cond->condition = '(  platform_id = ' . $platform->id . ' or platform_id is NUll  ) and type="' . $type . '"';
            $temp = PostTemplate::model()->find($cond);

            if (!empty($temp)) {
                if($this->debug)
                    echo '[generate] : $cond->condition = \'(  platform_id = \' . $platform->id . \' or platform_id is NUll  ) and type="\'.$type.\'"\';' . PHP_EOL . PHP_EOL . PHP_EOL;
                return $temp;
            }


            $temp = PostTemplate::model()->findByAttributes(array(
                'platform_id' => $platform->id,
                'catgory_id' => $category,
            ));

            if (!empty($temp)) {
                if($this->debug)
                    echo '[generate] :  \'platform_id\' => $platform->id,\'catgory_id\' => $category,' . PHP_EOL . PHP_EOL . PHP_EOL;
                return $temp;
            }

            $cond = new CDbCriteria();
            $cond->order = 'RAND()';
            $cond->condition = '( ( platform_id = ' . $platform->id . ' or platform_id is NUll ) and  catgory_id is NULL ) and  type="' . $type . '"';
            $temp = PostTemplate::model()->find($cond);


            if (!empty($temp)) {
                if($this->debug)
                    echo '[generate] : $cond->condition = \'( ( platform_id = \' . $platform->id . \' or platform_id is NUll ) and  catgory_id is NULL ) and  type="\'.$type.\'"\';' . PHP_EOL . PHP_EOL . PHP_EOL;
                return $temp;
            }
        }else {


            $cond = new CDbCriteria();
            $cond->order = 'RAND()';
            $cond->condition = '( ( platform_id = ' . $platform->id . ' or platform_id is NUll ) and  catgory_id ='.$category.' ) and type !="Video"';
            $temp = PostTemplate::model()->find($cond);


            if (!empty($temp)) {
                if($this->debug)
                    echo '[generate] :$cond->condition = \'( ( platform_id = \' . $platform->id . \' or platform_id is NUll ) and  catgory_id =\'.$category.\' ) and type !="Video"\';' . PHP_EOL . PHP_EOL . PHP_EOL;
                return $temp;
            }


            $cond = new CDbCriteria();
            $cond->order = 'RAND()';
            $cond->condition = '( ( platform_id = ' . $platform->id . ' or platform_id is NUll ) and  catgory_id is NULL ) and type !="Video"';
            $temp = PostTemplate::model()->find($cond);


            if (!empty($temp)) {
                if($this->debug)
                    echo '[generate] :$cond->condition = \'( ( platform_id = \' . $platform->id . \' or platform_id is NUll ) and  catgory_id is NULL ) and type !="Video" \';' . PHP_EOL . PHP_EOL . PHP_EOL;
                return $temp;
            }

            $cond = new CDbCriteria();
            $cond->order = 'RAND()';
            $cond->condition = '( ( platform_id = ' . $platform->id . ' or platform_id is NUll ) and  catgory_id is NULL )';
            $temp = PostTemplate::model()->find($cond);


            if (!empty($temp)) {
                if($this->debug)
                    echo '[generate] : $cond->condition = \'( ( platform_id = \' . $platform->id . \' or platform_id is NUll ) and  catgory_id is NULL )\';' . PHP_EOL . PHP_EOL . PHP_EOL;
                return $temp;
            }
        }

        if ($platform->title == 'Instagram') {
            isset($temp->id) ? $temp->id : null;
        }

        return Yii::app()->params['templates'];

    }

    public function update_items_generator($news,$previous_posts){

            $parent = null;

            $category = null;
            $valid_twitter= false;

            if(isset($news->category->title))
                $category = '#' . trim(str_ireplace(array('!', '"', ':', '.', '..', '...', ' '), '_', $news->category->title));

            $sub_category = null;

        $settings = Settings::model()->findByPk(1);

            $cdb = new CDbCriteria();

            $cdb->condition = 'news_id = '.$news->id.' and type != "video" and type !="gallery"';

            $data_media_original  = MediaNews::model()->find($cdb);

            $twitter_id = $this->get_platform_update();

            $facebook_id = $this->get_platform_update('Facebook');
            if($settings->enable_download_videos == 1) {
                if ($this->get_video_in_news($news->id)) {


                    if ($this->debug)
                        echo '[generate] : now in get video in news ' . PHP_EOL;

                    $valid_twitter = true;

                    list($data_media, $valid, $path) = $this->get_media_news($news);

                    if ($valid) {

                        if ($this->debug)
                            echo '[generate] : now in get media news is valid  : -> ' . $valid . '  in news ' . PHP_EOL;


                        if (!empty($data_media))
                            $data_media_original = $data_media;

                        if ($this->video_size($path)) {


                            if ($this->debug)
                            {
                                echo '[generate] : now in video size is valid  : -> ' . $valid . '  in news ' . PHP_EOL;
                            }
                            $valid_twitter = false;


                        }
                    } else {
                        if ($this->debug)
                            echo '[generate] : now in get media news is valid  : -> ' . $valid . '  in news ' . PHP_EOL;
                    }


                    if (isset($path)) {

                        if ($this->debug)
                            echo '[generate] : Delete file from server {{ ' . $path . ' }}' . PHP_EOL;
                        $this->delete_file_from_server($path);
                    }
                }
            }else{
                if($this->debug)
                echo 'video downloader is disabled';
            }


            if(empty($data_media_original)){

                $cdb = new CDbCriteria();

                $cdb->condition = 'news_id = '.$news->id.' and type != "video"';

                $data_media_original =  MediaNews::model()->find($cdb);
            }


            foreach ($previous_posts as $item) {

                if($item->is_posted==1 || $item->is_scheduled == 0){
                    continue;
                }
                $is_scheduled=0;
                $time="";
                $new_shorten=$news->link;

                if(!empty($news->link)){

                    switch ($item->platforms->title){
                        case 'Facebook':
                            $new_shorten .= '?utm_source=facebook&utm_medium=referral&utm_campaign=sortechs';
                            break;
                        case 'Twitter':
                            $new_shorten .='?utm_source=twitter&utm_medium=referral&utm_campaign=sortechs';
                            break;

                        case 'Instagram':
                            $new_shorten .='?utm_source=instagram&utm_medium=referral&utm_campaign=sortechs';
                            break;
                    }
                }

                if(!empty($new_shorten)){

                    $new_shorten = $this->GoogleShort($new_shorten);
                }elseif(!empty($news->shorten_url)){
                    $new_shorten=$news->shorten_url;
                }

                $media = null;

                if($item->platforms->title == "Twitter" && $valid_twitter)
                    $data_media_original = $this->get_media_for_twitter($news);

                if($item->platforms->title == "Instagram") {
                    $data_media_original = $this->get_media_for_instagram($news);
                }

                $temp = $this->get_template($item->platforms, $news->category_id, isset($data_media_original->type)?$data_media_original->type:"Image");


                $title = $news->title;


                $description = $news->description;

                if ($item->platforms->title != 'Instagram') {
                    $title = str_ireplace($this->hash_tag, $this->trim_hash, $news->title);
                    $description = str_ireplace($this->hash_tag, $this->trim_hash, $news->description);
                }

                $title = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $title);

                $title = preg_replace('/\n(\s*\n){2,}/', "\n\n", $title);

                $description = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $description);

                $description = preg_replace('/\n(\s*\n){2,}/', "\n\n", $description);

                $shorten_url = $news->shorten_url;

                $full_creator = $news->creator;

                $text = str_ireplace(
                    array('[title]', '[description]', '[short_link]', '[author]'),
                    array($title, $description, $shorten_url, $full_creator),
                    $temp['text']
                );

                if ($item->platforms->title == 'Facebook' or $item->platforms->title == 'Twitter') {

                    $text = str_ireplace('# ', '#', $text);

                    $found = true;

                    preg_match_all("/#([^\s]+)/", $text, $matches);

                    if (isset($matches[0])) {

                        $matches[0] = array_reverse($matches[0]);
                        $count = 0;
                        foreach ($matches[0] as $hashtag) {


                            if(strpos($text,'[section]')) {
                                if ($count >= 1) {
                                    $text = str_ireplace($hashtag, str_ireplace('_', ' ', str_ireplace('#', '', $hashtag)), $this->clear_tags($text));
                                    break;
                                }
                            }else{
                                if ($count >= 2) {
                                    $found = false;
                                    $text = str_ireplace($hashtag, str_ireplace('_', ' ', str_ireplace('#', '', $hashtag)), $text);
                                }
                            }
                            $count++;
                        }
                        $found =true;
                        if ($count >= 2)
                            $found = false;
                    }


                    if ($found)
                        $text = str_ireplace(array('[section]', '[sub_section]'), array($category, $sub_category), $text);
                    else
                        $text = str_ireplace(array('[section]', '[sub_section]', '|'), array('', '', '',), $text);

                } elseif ($item->platforms->title == 'Instagram') {
                    $text = str_ireplace(array('[section]', '[sub_section]', '|'), array('', '', ''), str_ireplace('# ', '#', $text));

                }




                if($this->debug)
                    echo '[Generator] : Now generate '.$item->platforms->title.' , Time : '.$time.PHP_EOL;


                if ($item->platforms->title == 'Twitter') {

                    $text_twitter = $text;
                    if ($temp['type'] == 'Preview')
                        if (!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!', $text, $matches))
                            $text = $text . PHP_EOL . $news->shorten_url;


                    if ($this->getTweetLength($text_twitter, $temp['type'] == 'Image' ? true : false, $temp['type'] == 'Video' ? true : false) > 141) {
                        $is_scheduled = 0;
                    } else {
                        $text = $text_twitter . PHP_EOL;
                        if ($temp['type'] == 'Preview')
                            if (!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!', $text, $matches))
                                $text .= PHP_EOL . $news->shorten_url;
                    }
                }


                $text = $this->clear_tags($text);


                if ($item->platforms->title != 'Instagram') {

                    $text = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $text);
                    $text = preg_replace('/\n(\s*\n){2,}/', "\n\n", $text);


                    if($this->image_instagram_scheduled){

                        if($this->debug)
                            echo '[Generator] : is scheduled after instagram : '.$is_scheduled.PHP_EOL;

                        $this->image_instagram_scheduled = true;

                    }

                    if($media == 'http://www.thenational.ae/styles/images/default_social_share.jpg')
                        $this->image_instagram_scheduled = true;


                }



                if($this->debug)
                    echo '[Generator] : is scheduled  : '.$item->platforms->title.' '.$is_scheduled.PHP_EOL;


                if($this->image_instagram_scheduled)
                    $is_scheduled = 0;

                $PostQueue = new PostQueue();
                $PostQueue->setIsNewRecord(true);
                $PostQueue->id = null;
                $PostQueue->command = false;
                $PostQueue->type = $temp['type'];
                $PostQueue->post = trim($text);
                $PostQueue->schedule_date = $item->schedule_date;
                $PostQueue->catgory_id = $item->catgory_id;
                //$PostQueue->main_category_id = $item->main_category_id;
                $PostQueue->link = $new_shorten;
                $PostQueue->is_posted = 0;
                $PostQueue->news_id = $news->id;
                $PostQueue->post_id = null;
                $PostQueue->media_url = isset($data_media_original->media)?$data_media_original->media:$this->general_image;
                $PostQueue->settings = $item->settings;
                $PostQueue->is_scheduled = $item->is_scheduled;
                $PostQueue->platform_id = $item->platforms->id;
                $PostQueue->generated = 'auto';
                $PostQueue->created_at = date('Y-m-d H:i:s');
                $item->is_scheduled=0;
                $item->save(false);
                if ($parent == null) {

                    $PostQueue->parent_id = null;

                    if ($PostQueue->save(false)){
                        $parent = $PostQueue->id;

                    }
                    else
                        $this->send_email('Error on GeneratorsCommand.php ', 'error on post queue save  <a href="' . Yii::app()->params['domain'] . '/postQueue/' . $PostQueue->id . '"></a>');

                } else {
                    $PostQueue->parent_id = $parent;
                    if (!$PostQueue->save(false))
                        $this->send_email('Error on GeneratorsCommand.php ', 'error on post queue save  <a href="' . Yii::app()->params['domain'] . '/postQueue/' . $PostQueue->id . '"></a>');
                }


            }

            $news->generated = 1;
            if (!$news->save())
                $this->send_email('Error on GeneratorsCommand.php ', 'error on post queue save  <a href="' . Yii::app()->params['domain'] . '/news/' . $news->id . '"></a>');




    }
    public function get_details($link,$id)
    {
        $html = new SimpleHTMLDOM();
        $html_url = $html->file_get_html(trim($link));
        $page_details = PageDetails::model()->findAllByAttributes(array('source_id' => $id));
        $data['media']['image'] = 'http://www.thenational.ae/styles/images/default_social_share.jpg';
        $data['body_description'] = "";
        $data['description'] = "";
        $data['title'] = "";
        $data['creator'] = "";
        if(isset($html_url) && !empty($html_url)) {
            if (!empty($page_details)) {
                foreach ($page_details as $detail) {
                    if (isset($detail) && !empty($detail)) {

                        //Author
                        if (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'creator') {
                            $data['creator'] = trim($this->clear_tags(trim($html_url->find($detail->predication, 0)->content)));

                        } elseif (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'creator') {
                            $data['creator'] = trim($this->clear_tags(trim($html_url->find($detail->predication, 0)->plaintext)));
                        }
                        //End author

                        //title
                        if (empty(trim($data['title']))) {
                            if (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'title') {
                                $data['title'] = trim($this->clear_tags(trim($html_url->find($detail->predication, 0)->plaintext)));
                            }
                            if ((empty(trim($data['title']))) && isset($html_url->find($detail->predication, 1)->plaintext) and !empty($html_url->find($detail->predication, 1)->plaintext)) {
                                $data['title'] = $this->clear_tags(trim($html_url->find($detail->predication, 1)->plaintext));
                            }
                        }
                        //End title


                        //Body desc
                        if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'body_description') {
                            $body_desc = $html_url->find($detail->predication);
                            if (isset($body_desc)) {
                                foreach ($body_desc as $bodies) {
                                    $data['body_description'] .= $this->clear_tags(trim($bodies->plaintext));
                                }
                            }
                        } elseif (!empty($html_url->find($detail->predication, 0)->plaintext) and isset($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'body_description') {
                            $data['body_description'] = $this->clear_tags(trim($html_url->find($detail->predication, 0)->plaintext));
                        }


                        //End body desc

                        //Description
                        if($detail->pageType->title == 'description') {
                            if (empty(trim($data['description']))) {
                                for ($i = 0; $i < 10; $i++) {
                                    if (isset($html_url->find($detail->predication, $i)->plaintext) && !empty($html_url->find($detail->predication, $i)->plaintext)) {
                                        $data['description'] = $this->clear_tags(trim($html_url->find($detail->predication, $i)->plaintext));
                                        if (empty(trim($data['description'])) || strlen(trim($data['description'])) < 50) {
                                            continue;
                                        } else {
                                            break;
                                        }
                                    }
                                }
                            }
                        }


                        //End Description

                        //Publishing date
                        if (isset($html_url->find($detail->predication, 0)->datetime) and !empty($html_url->find($detail->predication, 0)->datetime) and $detail->pageType->title == 'publishing_date') {
                            $data['publishing_date'] = $this->get_date($html_url->find($detail->predication, 0)->datetime);
                        }
                        if (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'publishing_date') {
                            $data['publishing_date'] = $this->get_date($html_url->find($detail->predication, 0)->plaintext);
                        } elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'publishing_date') {
                            $data['publishing_date'] = $this->get_date($html_url->find($detail->predication, 0)->content);
                        }
                        //End publishing date

                        //image
                        if (isset($html_url->find($detail->predication, 0)->src) and !empty($html_url->find($detail->predication, 0)->src) and $detail->pageType->title == 'image') {
                            if (strpos($html_url->find($detail->predication, 0)->src, 'http') === 0) {
                                $data['media']['image'] = $html_url->find($detail->predication, 0)->src;

                            } else
                                $data['media']['image'] = Yii::app()->params['feedUrl'] . $html_url->find($detail->predication, 0)->src;
                        } elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'image') {
                            if (strpos($html_url->find($detail->predication, 0)->content, 'http') === 0)
                                $data['media']['image'] = $html_url->find($detail->predication, 0)->content;
                            else
                                $data['media']['image'] = Yii::app()->params['feedUrl'] . $html_url->find($detail->predication, 0)->content;

                        }
                        //end image

                        //video
                        if (isset($html_url->find($detail->predication, -1)->src) and !empty($html_url->find($detail->predication, -1)->src) and $detail->pageType->title == 'video') {
                            $data['media']['video'] = $html_url->find($detail->predication, -1)->src;
                        } elseif (isset($html_url->find($detail->predication, -1)->content) and !empty($html_url->find($detail->predication, -1)->content) and $detail->pageType->title == 'video') {
                            $data['media']['video'] = $html_url->find($detail->predication, -1)->content;

                        } else if (isset($html_url->find($detail->predication, -1)->data) and !empty($html_url->find($detail->predication, -1)->data) and $detail->pageType->title == 'video') {
                            $data['media']['video'] = $html_url->find($detail->predication, -1)->data;
                        }
                        //end video

                        //gallery
                        $counter = 0;
                        if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'gallery') {
                            $gall = $html_url->find($detail->predication);
                            foreach ($gall as $item) {
                                if (strpos($item->src, 'http') === 0) {
                                    if ($data['media']['image'] != $item->src)
                                        $data['media']['gallery'][$counter]['src'] = $item->src;
                                } else {
                                    if ($data['media']['image'] != Yii::app()->params['feedUrl'] . $item->src)
                                        $data['media']['gallery'][$counter]['src'] = Yii::app()->params['feedUrl'] . $item->src;
                                }

                                $data['media']['gallery'][$counter]['caption'] = $this->clear_tags($item->parent()->parent()->last_child()->plaintext);
                                $counter++;
                            }

                        }
                        //End gallery

                        //Tags
                        if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'tags') {
                            $tags_counter = $html_url->find($detail->predication);
                            foreach ($tags_counter as $tag) {
                                $tag_space = trim($tag->plaintext);
                                $tag_space = preg_replace("/[\s_]/", "_", $tag_space);
                                $data['tags'][] = $this->clear_tags($tag_space);
                            }

                        }
                        //Tags
                        //link

                        //end links
                    }
                }
                if (!empty($html_url->find('meta[property=og:url]', 0)->content)) {
                    $data['link'] = $html_url->find('meta[property=og:url]', 0)->content;
                }
                if(empty(trim($data['description']))){
                    if(isset($html_url->find('meta[property=og:description]', 0)->content) && !empty($html_url->find('meta[property=og:description]', 0)->content))
                        $data['description'] = $this->clear_tags(trim($html_url->find('meta[property=og:description]', 0)->content));
                }
                if(empty(trim($data['title']))){
                    if(isset($html_url->find('meta[property=og:title]', 0)->content) && !empty($html_url->find('meta[property=og:title]', 0)->content))
                        $data['title'] = $this->clear_tags(trim($html_url->find('meta[property=og:title]', 0)->content));
                }
            }
            return $data;
        }
        return null;
    }
    public function AddMediaNews_update($news_id, $type, $data)
    {


        $id = null;

        $find = true;

        if ($type == 'gallery') {
            foreach ($data as $item) {
                if(isset($item['caption']) && !empty($item['caption']) && $find){
                    foreach($this->image_copyright as $query) {
                        if(!empty(stristr($item['caption'], trim($query)))){
                            $find = false;
                            $this->image_instagram_scheduled = true;
                        }
                    }
                }
                if (empty(MediaNews::model()->findByAttributes(array('media' => $item['src'], 'news_id' => $news_id, 'type' => $type)))) {
                    $media = new MediaNews();
                    $media->type = $type;
                    $media->media = $item['src'];
                    $media->news_id = $news_id;
                    $media->setIsNewRecord(true);
                    $media->save(false);
                    if ($id == null)
                        $id = $media->id;
                }
            }
            return $id;
        }

        if (empty(MediaNews::model()->findByAttributes(array('media' => $data, 'news_id' => $news_id, 'type' => $type)))) {
            $media = new MediaNews();
            $media->type = $type;
            $media->media = $data;
            $media->news_id = $news_id;
            $media->setIsNewRecord(true);
            $media->save(false);
            if ($id == null)
                $id = $media->id;
        }
        return $id;


    }

    public function check_and_update_news_url($debug){
        if($debug)
            echo 'Now get scheduled news'.PHP_EOL;
        $get_all_news_data = $this->total_news_posts_query(date('Y-m-d 00:00:00'),date('Y-m-d 23:59:59'));
        if(!empty($get_all_news_data)) {
            foreach ($get_all_news_data as $item) {
                $data = $this->get_details($item['link'], $item['category_id']);
                if(empty($data) || !isset($data)){
                    continue;
                }
                if ($debug)
                    echo 'Save new news if it is a new link' . PHP_EOL;
                if (!empty($data['link'])) {
                    if ($data['link'] != $item['link']) {
                        if ($debug)
                            echo 'saving new news' . PHP_EOL;
                        $news = new News();
                        $news->attributes = $item;
                        $news->id = null;
                        $news->link = $data['link'];
                        $news->description = $data['description'];
                        $news->title = $data['title'];
                        $news->link_md5 = md5($data['link']);
                        $news->parent_news_id = $item['id'];
                        $news->setIsNewRecord(true);
                        if ($news->save(false)) {
                            if (isset($data['media']['video']))
                                $id = $this->AddMediaNews_update($news->id, 'video', $data['media']['video']);


                            if (isset($data['media']['gallery']))
                                $id = $this->AddMediaNews_update($news->id, 'gallery', $data['media']['gallery']);
                            if (isset($data['media']['image']))
                                $id = $this->AddMediaNews_update($news->id, 'image', $data['media']['image']);

                            $post_queue = PostQueue::model()->findAllByAttributes(array('news_id' => $item['id']));
                            $this->update_items_generator($news,$post_queue);


                        } else {
                            if ($debug)
                                echo 'Not saved' . PHP_EOL;
                        }
                    } else {
                        if ($debug)
                            echo 'Same link no changes performed' . PHP_EOL;
                    }
                } else {
                    if ($debug)
                        echo 'empty link' . PHP_EOL;
                }

            }
        }else{
            if ($debug)
                echo 'There is no news to be updated' . PHP_EOL;
        }

    }
    public function check_page_for_link_only($url){
        $html = new SimpleHTMLDOM();
        $html_data = $html->file_get_html($url);
        if(isset($html_data)) {
            if (isset($html_data->find('meta[property=og:url]', 0)->content) and !empty($html_data->find('meta[property=og:url]', 0)->content)) {
                return $html_data->find('meta[property=og:url]', 0)->content;
            } elseif (isset($html_data->find('meta[name=twitter:url]', 0)->content) and !empty($html_data->find('meta[name=twitter:url]', 0)->content)) {
                return $html_data->find('meta[property=twitter:url]', 0)->content;
            }
        }
        return false;
    }
    public function total_news_posts_query($from,$to){
        $news_date = date('Y-m-d');
        return Yii::app()->db->createCommand("SELECT distinct news.* FROM `news` INNER JOIN post_queue on news.id = post_queue.news_id WHERE post_queue.`schedule_date` >='$from' and post_queue.`schedule_date` <='$to' and  DATE_FORMAT(news.`created_at`,'%Y-%m-%d') = '$news_date' and post_queue.is_scheduled=1")->queryAll();

    }
    public function GoogleShort($url){

        $shortUrl = new ShortUrl();

        $url_short =  $shortUrl->short(urldecode($url));

        if(!empty($url_short)){
            return $url_short;
        }

        return null;
    }


    protected function LoadPostTemplate(){

        return PostTemplate::model()->findAll('deleted = 0');
    }

    protected function LoadPlatForm(){

        return CHtml::listData(Platform::model()->findAll('deleted = 0 '),'id','title');
    }

    protected function GetMedia($id){
        return MediaNews::model()->findAll('news_id = '.$id);
    }



    protected function checkCategory($title){

        return Category::model()->findByAttributes(
            array(
                'title'=>$title
            )
        );
    }

    protected function checkSubCategory($title){
        return SubCategories::model()->findByAttributes(
            array(
                'title'=>$title
            )
        );
    }

    protected function make_image_square($image, $square = 0)
    {
        $mask = new \Imagick();
        $mask->readImage($image);
        $mask->setImageResolution(144,144);
        $d = $mask->getImageGeometry();
        $h = $d['height'];
        $w = $d['width'];
        $type = $h > $w?'height':'width';
          $q = ($d[$type]/2)/2;
        $obj = new Imagick();
        $obj->newImage($d[$type], $d[$type], new ImagickPixel('#d7d6db'));
        $obj->setImageResolution(144,144);
        $obj->setImageFormat('png');
        $obj->compositeImage($mask, Imagick::COMPOSITE_DEFAULT,0,$q);
        $obj->writeImage($image);
        /*
        $base = new \Imagick();
        $base->readImage($image);
        $base->setImageResolution(144,144);
        $base->resampleImage  (288,288,imagick::FILTER_UNDEFINED,1);
        if ($square)
        {
            $d = $base->getImageGeometry();
            $h = $d['height'];
            $w = $d['width'];
            $base_image = $h > $w?$w:$h;
            $base->cropImage($base_image, $base_image,0,0);
            //$base->resizeImage(700, 700, Imagick::FILTER_LANCZOS, 1);
        }
        $base->setImageResolution(144,144);
        //$base->setImageResolution(1000,1500);


        $base->writeImage($image);*/
    }


    protected function Month($index){

        $months = array(
            "01" => "يناير",
            "02" => "فبراير",
            "03" => "مارس",
            "04" => "أبريل",
            "05" => "مايو",
            "06" => "يونيو",
            "07" => "يوليو",
            "08" => "أغسطس",
            "09" => "سبتمبر",
            "10" => "أكتوبر",
            "11" => "نوفمبر",
            "12" => "ديسمبر"
        );

        return array_search($index,$months);
    }

    protected function LoadTemplate($id_category){

    }

    protected function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public function getTweetLength($tweet, $gotImage ,$video) {
        $tcoLengthHttp = 23;
        $tcoLengthHttps = 23;
        $twitterPicLength = 24;
        $twitterVideoLength = 24;
        $urlData = Twitter_Extractor::create($tweet)->extract();
        $tweetLength = mb_strlen($tweet,'utf-8');
        foreach ($urlData['urls_with_indices'] as $url) {
            $tweetLength -= mb_strlen($url['url']);
            $tweetLength += mb_stristr($url['url'], 'https') === 0 ? $tcoLengthHttps : $tcoLengthHttp;
        }
        if ($gotImage) $tweetLength += $twitterPicLength;
        if ($video) $tweetLength += $twitterVideoLength;
        return $tweetLength;
    }

    public function shorten($input, $length, $ellipses = true, $strip_html = true) {
        if ($strip_html) {
            $input = strip_tags($input);
        }
        if (mb_strlen($input,'UTF-8') <= $length) {
            return $input;
        }
        $last_space = strrpos(substr($input, 0, $length), ' ');
        $trimmed_text = mb_substr($input, 0, $last_space);
        if ($ellipses) {
            $trimmed_text .= '...';
        }
        return $trimmed_text;
    }

    public function shorten_point($input, $point = '.') {
        $input_ex = explode($point,$input);
        $data = array_chunk($input_ex,2);
        if(is_array($data)){

            return (implode('. '.PHP_EOL,$data[0])).'. ';
        }
        return $input;
    }
    /*

   public function shorten_point($input, $point = '.') {
       $input_ex = explode($point,$input);
       $data = array_chunk($input_ex,5);
       if(is_array($data)){
           $data =  implode('. '.PHP_EOL,$data[0]);
           if(mb_strlen($data,'utf-8') > 2000){
               $data = array_chunk($input_ex,3);
               if(is_array($data)){
                   $data =  implode('. '.PHP_EOL,$data[0]);
                   if(mb_strlen($data,'utf-8') > 2000){
                       $data = array_chunk($input_ex,1);
                       return  $data =  implode('. '.PHP_EOL,$data[0]);
                   }
               }
               return $data;
           }
           return $data;
       }
       return $input;
   }
   */
    public function send_email($title,$message){

        $mail = new JPhpMailer;

        $mail->SMTPAuth = true;
        /* $mail->Username = 'Admin';
         $mail->Password = '123';*/
        $mail->SetFrom('system@sortechs.com', 'System');
        $mail->Subject = 'Error - '.$title;
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
        $msg_contents_header = '<h2>'.$title.'</h2>';
        $msg_contents_body = '<h3>'.$message.'<br /><br />Sincerely<br />Sortechs team';
        $mail->MsgHTML($msg_contents_header.$msg_contents_body);
        /*        $mail->AddAttachment('http://www.anazahra.com/wp-content/gallery/lamia-abdeen-abayas14062016/ubuntu-tribe-collection-14.jpg','Pic');*/
        foreach (Yii::app()->params['email'] as $param) {
            $mail->AddAddress($param, 'Mohammad');
        }
        $mail->Send();
    }
    public function send_Pdf_email($user_downloaded_pdf,$name,$l_name,$email,$subject,$MSGHTML,$link,$from_date,$to_date,$define_smart,$system_name){

        $mail = new JPhpMailer;
if($define_smart){
    define(`SMART_HOST`, Yii::app()->params['email_send']['Host']);

}
        $mail->IsSMTP();


        $mail->SMTPAuth = true;
//        $mail->SMTPSecure = 'ssl';
        $mail->Host = Yii::app()->params['email_send']['Host'];//'mail.sortechs.com';
        //Muy importante para que llegue a hotmail y otros
        $mail->Helo = Yii::app()->params['email_send']['From_email'];//"sortechs.com";
        $mail->ConfirmReadingTo = Yii::app()->params['email_send']['From_email'];
        $mail->Username = Yii::app()->params['email_send']['Username'];//'noreply@sortechs.com';
        $mail->Password = Yii::app()->params['email_send']['Password'];//'N@sortechs123';
        $mail->SetFrom(Yii::app()->params['email_send']['From_email'], Yii::app()->params['email_send']['From_name']);
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
        /*$msg_contents_body = '<h1>'.$system_name.'</h1><h2>'.$name." ".$l_name." Content calender file".'[ '.$from_date. ' _ '.$to_date .']</h2>';*/
        $msg_contents_body = '<h1>'.$system_name.'</h1><h2>'.$name." ".$l_name." Content calender file".'[ '.$from_date. ' _ '.$to_date .']</h2>'.'<h3>Hello '.$name." ".$l_name.'</h3>Your content calender for the period ['.$from_date. ' _ '.$to_date .'] is now ready. Click on the button below to be taken to it:<br /><br />'.'<a href="'.$link.'" style="border:1px solid black;padding:8px;color:white;background-color:#8064A2;text-decoration:none;margin-top:10px;margin-bottom:10px;border-radius:8px;">'.$MSGHTML.'</a><br /><br />Sincerely<br />Sortechs team';
        $mail->MsgHTML($msg_contents_body);

        $mail->AddAddress($email, $name);
        $mail->Send();

    }


    public function crop_image($number , $type ,$mask){
        //split the cropping for to sides
        $number = ceil($number/2);

        //get the image dimensions
        $size=$mask->getImageGeometry();
        $w = $size['width'];
        $h = $size['height'];
        //dimensions ends here

        if($type == 'h'){
            $mask->cropImage(0, $size['height']-$number,0,$number);
            $mask->cropImage(0, $size['height']-$number,0,0);

        }else{
            $mask->cropImage($size['width']-$number, 0,$number,0);
            $mask->cropImage($size['width']-$number, 0,0,0);
        }
        return $mask;
    }

    public function scaleImage($imagePath) {
        $imagick = new \Imagick();
        $imagick->readImage($imagePath);
        $imagick->setImageResolution(144,144);

        //get the image dimensions
        $size=$imagick->getImageGeometry();

        $w = $size['width'];
        $h = $size['height'];
        $big=$w>$h?$w:$h;
        $big_type=$w>$h?'w':'h';
        //dimensions ends here

        //crop image with the extra size if exist
        $extra_size_counter=0;

        //images must have an aspect ratio between the from and to
        $from=ceil((1/1.91)*$big);
        $to=floor((4/5)*$big);
        //extra size for image if not compatible with instagram
        $extra_size=10;

        if($big_type=='w'){
            if($h<$from){
                //make the image compatible with instagram
                while (true){
                    $extra_size_counter+=$extra_size;
                    $big+=$extra_size;
                    //$imagick->scaleImage($big, 0);
                    $imagick->resizeImage($big, 0,imagick::FILTER_LANCZOS, 1);
                    $imagick->setImageResolution(144,144);
                    $size=$imagick->getImageGeometry();
                    if($size['height']>=$from){
                        break;
                    }
                }
            }
        }else{
            if($w<$to){
                //make the image compatible with instagram
                //$imagick->scaleImage($to, 0);
                $imagick->resizeImage($to, 0,imagick::FILTER_LANCZOS, 1);
                $size=$imagick->getImageGeometry();
                $extra_size_counter=$size['height']-$big;

            }
        }

        //fix the resolution


        if($extra_size_counter){
            //crop image if needed
            $imagick=$this->crop_image($extra_size_counter , $big_type ,$imagick);
        }
        //fix the resolution




        $imagick->writeImage($imagePath);
        return $imagick;
    }



    public function scaleImage_new_resolution_problem($imagePath) {
        $imagick = new \Imagick();
        $imagick->readImage($imagePath);
        $imagick->setImageCompressionQuality(100);
        $imagick->setImageResolution(144,144);

        //get the image dimensions
        $size=$imagick->getImageGeometry();

        $w = $size['width'];
        $h = $size['height'];
        $big=$w>$h?$w:$h;
        $big_type=$w>$h?'w':'h';
        //dimensions ends here

        //crop image with the extra size if exist
        $extra_size_counter=0;

        //images must have an aspect ratio between the from and to
        $from=ceil((1/1.91)*$big);
        $to=floor((4/5)*$big);
        //extra size for image if not compatible with instagram
        $extra_size=10;

        if($big_type=='w'){
            if($h<$from){


                if($big>1080){
                    $imagick=$this->resize_image(1080,0,$imagick);

                }

                $resize=ceil(($big-$h)/4);
                //echo $resize;die;
                $imagick=$this->resize_image($big-$resize,0,$imagick);


                $size=$imagick->getImageGeometry();

                $w = $size['width'];
                $h = $size['height'];
                $big=$w>$h?$w:$h;
                $from=ceil((1/1.91)*$big);

                //make the image compatible with instagram
                $imagick=$this->resize_image(0,$from,$imagick);

                $size=$imagick->getImageGeometry();
                $extra_size_counter=$size['width']-$big;

            }
        }else{
            if($w<$to){
                //
                if($big>1350){
                    $imagick=$this->resize_image(0,1350,$imagick);
                }

                $resize=ceil(($big-$h)/4);
                $imagick=$this->resize_image(0,$big-$resize,$imagick);
                $size=$imagick->getImageGeometry();

                $w = $size['width'];
                $h = $size['height'];
                $big=$w>$h?$w:$h;
                $to=ceil((1/1.91)*$big);

                //make the image compatible with instagram
                $imagick=$this->resize_image($to,0,$imagick);
                $size=$imagick->getImageGeometry();
                $extra_size_counter=$size['height']-$big;

            }
        }

        //fix the resolution


        if($extra_size_counter){
            //crop image if needed
            $imagick=$this->crop_image($extra_size_counter , $big_type ,$imagick);
        }
        //fix the resolution




        $imagick->writeImage($imagePath);

        return $imagick;
    }



    public function resize_image($w,$h,$obj){
        //$obj->scaleImage($w, $h);
        $obj->resizeImage($w, $h,imagick::FILTER_LANCZOS, 1);

        return $obj;
    }

    protected function clear_tags($string){

        $string = strip_tags($string);
        $string = str_replace('&nbsp;','',$string);
        $string = str_replace('&raquo;','',$string);
        $string = str_replace('&laquo;','',$string);
        $string = str_replace('&quot;','',$string);
        $string = str_replace('nbsp;','',$string);
        $string = str_replace('&#x2019;','’',$string);
        $string = str_replace('&#x2013;','-',$string);
        $string = str_replace('&#x2018;','',$string);
        $string = str_replace('&#x2014;','',$string);
        $string = str_replace('&#ad;','',$string);
        $string = str_replace('&#xf8;','',$string);
        $string = str_replace('&#xe9;','é',$string);
        $string = str_replace('&#xef;','ï',$string);
        $string = str_replace('&#xb0;','°',$string);
        $string = str_replace('&#xa0;',' ',$string);
        $string = str_replace('&#x2022;','•',$string);
        $string = str_replace('&#xe4;','ä',$string);
        $string = str_replace('&#xad;','',$string);
        $string = str_replace('&#xa3;','£',$string);
        return $string;
    }
    protected function get_date($date){

        return date('Y-m-d H:i:s',strtotime($date));

    }

    protected function clear_author($string){

        $string = strip_tags($string);

        return $string;
    }
    protected function getAllNews(){

        $news=News::model()->model()->get_today_news();
        $all_news=array();
        foreach ($news as $one){
            $all_news[$one->id]=$one;
        }

        return $all_news;
    }


    protected function check_and_update_news($id,$post_id,$platform_id){

        $this->platform_id=$platform_id;
        $this->post_id=$post_id;
        $this->news = News::model()->findByPk($id);


        $this->details=$this->get_news_details($this->news->link,$this->news->category->id);

        if($this->news->title != $this->details['title']){
            $this->category=Category::model()->findByPk($this->news->category_id);
            $this->sub_category=SubCategories::model()->findByPk($this->news->sub_category_id);

            $this->news->title = $this->details['title'];
            $this->news->column = $this->details['column'];
            $this->news->description = isset($this->details['description'])?$this->details['description']:null;
            $this->news->creator = isset($this->details['author'])?$this->details['author']:null;
            //$this->news->schedule_date = $this->model->time;
            if($this->news->save(true)) {
                $this->generator();
            }

        }
    }
    protected function mediaNews($id, $data)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('news_id',$id);
        MediaNews::model()->deleteAll($criteria);


        $media = new MediaNews();
        if($data['type'] == 'image'){

            $media->command=false;
            $media->id=null;
            $media->news_id = $id;
            $media->media = $data['src'];
            $media->type=$data['type'];
            $media->setIsNewRecord(true);
            if(!$media->save()){

            }


        }elseif($data['type'] == 'gallery'){
            foreach ($data['src'] as $item) {
                $empty_media = MediaNews::model()->findByAttributes(array('news_id'=> $id,'media'=>$item));
                if(empty($empty_media)){
                    $media->id=null;
                    $media->news_id = $id;
                    $media->media = $item;
                    $media->type=$data['type'];
                    $media->setIsNewRecord(true);
                    if(!$media->save()){

                    }
                }

            }
        }

    }

    protected function generator(){

        $d = Hashtag::model()->findAll('deleted = 0');

        $this->trim_hash = array();

        $this->hash_tag = array();

        foreach ($d as $index=>$item) {
            $this->trim_hash[$index]=" #".str_replace(" ", "_",trim($item->title)).' ';
            $this->hash_tag[$index]=' '.trim($item->title).' ';
        }

        $this->parent = null;



        if(isset($this->category->title))
            $this->category = '#'.trim(str_replace(array('!','"',':','.','..','...',' '), '_',$this->category->title));


        if(isset($this->sub_category->title))
            $this->sub_category = '#'.trim(str_replace(array('!','"',':','.','..','...',' '), '_', isset($this->sub_category->title)));

        $this->source = Yii::app()->params['source'].' : ';

        if($this->category == '#'.'أعمدة')
            $this->source = Yii::app()->params['line'].' : ';


        $this->creator = false;

        if($this->news->creator == 'دبي - الإمارات اليوم')
            $this->creator =true;

        $this->platform = Platform::model()->findByPk($this->platform_id);
        $this->post();

        $this->news->generated = 1;

        $this->news->save();
    }

    private function get_templates($platform,$category){

        $temp  = PostTemplate::model()->findByAttributes(array(
            'platform_id'=>$platform->id,
            'catgory_id'=>$category,
        ));
        if(!empty($temp))
            return $temp;
        $cond = new CDbCriteria();
        $cond->order = 'RAND()';
        $cond->condition='( ( platform_id = '.$platform->id.' or platform_id is NUll ) and  catgory_id is NULL )';
        $temp  = PostTemplate::model()->find($cond);

        if(!empty($temp))
            return $temp;

        return Yii::app()->params['templates'];
    }
    private function get_news_details($link,$id)
    {

        $html = new SimpleHTMLDOM();
        $html_url = $html->file_get_html($link);
        $page_details = PageDetails::model()->findAllByAttributes(array('source_id'=>$id));
        $data = array();
        $data['body_description'] = "";
        foreach($page_details as $detail) {


            //title
            if (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'title') {
                foreach ($this->array_erem_words as $array_matches){
                    if (preg_match('/\b' . $array_matches . '\b/u',$html_url->find($detail->predication, 0)->content, $matches)) {
                        $data['title'] = str_replace($array_matches,'',$html_url->find($detail->predication, 0)->content);
                    }
                }
                if(!empty($data['title']))
                    $data['title'] = trim($this->clear_tags(trim($data['title'])));
                else
                    $data['title'] = trim($this->clear_tags(trim($html_url->find($detail->predication, 0)->content)));
            }
            elseif (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'title') {
                foreach ($this->array_erem_words as $array_matches){
                    if (preg_match('/\b' . $array_matches . '\b/u',$html_url->find($detail->predication, 0)->content, $matches)) {
                        $data['title'] = str_replace($array_matches,'',$html_url->find($detail->predication, 0)->content);
                    }
                }
                if(!empty($data['title']))
                    $data['title'] = trim($this->clear_tags(trim($data['title'])));
                else
                    $data['title'] = trim($this->clear_tags(trim($html_url->find($detail->predication, 0)->plaintext)));
            }


            //End title


            //Description
            if (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'description') {
                $data['description'] = $this->clear_tags(trim($html_url->find($detail->predication, 0)->plaintext));
            }
            elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'description') {
                $data['description'] = $this->clear_tags(trim($html_url->find($detail->predication, 0)->content));
            }


            //End Description
            if (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'creator') {
                $data['author'] = trim($this->clear_tags(trim($html_url->find($detail->predication, 0)->content)));

            }
            elseif (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'creator') {
                $data['author'] = trim($this->clear_tags(trim($html_url->find($detail->predication, 0)->plaintext)));
            }
            //End author
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'body_description') {
                $body_desc = $html_url->find($detail->predication);
                if (isset($body_desc)) {
                    foreach ($body_desc as $bodies) {
                        $data['body_description'] .= $this->clear_tags(trim($bodies->plaintext));
                    }
                }
            }elseif (!empty($html_url->find($detail->predication,0)->plaintext) and isset($html_url->find($detail->predication,0)->plaintext) and $detail->pageType->title == 'body_description') {
                $data['body_description'] = $this->clear_tags(trim($html_url->find($detail->predication,0)->plaintext));
            }

            //image
            if (isset($html_url->find($detail->predication, 0)->src) and !empty($html_url->find($detail->predication, 0)->src) and $detail->pageType->title == 'image') {
                if (strpos($html_url->find($detail->predication, 0)->src, 'http') === 0) {
                    $data['image']['src'] = $html_url->find($detail->predication, 0)->src;

                }else
                    $data['image']['src'] = Yii::app()->params['feedUrl'].$html_url->find($detail->predication, 0)->src;
            } elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'image') {
                if (strpos($html_url->find($detail->predication, 0)->content, 'http') === 0)
                    $data['image']['src'] = $html_url->find($detail->predication, 0)->content;
                else
                    $data['image']['src'] = Yii::app()->params['feedUrl'].$html_url->find($detail->predication, 0)->content;
                $data['image']['type']='image';
            }
            //end image


            //gallery
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'gallery') {
                $gall = $html_url->find($detail->predication);
                foreach ($gall as $item) {
                    if (strpos($html_url->find($detail->predication, 0)->src, 'http') === 0) {
                        if($data['image']['src'] != $item->src)
                            $data['gallary']['type']='gallery';
                        $data['gallary']['src'][] =$item->src;
                    }else {
                        if ($data['image']['src'] != Yii::app()->params['feedUrl']. $item->src)
                            $data['gallary']['type']='gallery';
                        $data['gallary']['src'][] = Yii::app()->params['feedUrl'].$item->src;
                    }
                }

            }
            //End gallery



            //Tags
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'tags') {
                $tags_counter = $html_url->find($detail->predication);
                foreach ($tags_counter as $tag) {
                    $tag_space = trim($tag->plaintext);
                    $tag_space = preg_replace("/[\s_]/", "_", $tag_space);
                    /*$body_description =preg_replace('/\b'.$tag->plaintext.'\b/u', '#'.$tag_space, $body_description);
                    $description =preg_replace('/\b'.$tag->plaintext.'\b/u', '#'.$tag_space, $description);*/

                    $data['tags'][] = $this->clear_tags($tag_space);
                }

            }
            //Tags
        }



        return $data;
    }

    protected function post(){

        $is_scheduled =1;

        $twitter_is_scheduled =false;

        $media= null;

        $temp = $this->get_template($this->platform,$this->news->category_id);

        $title = $this->news->title;

        $description = $this->shorten_point($this->news->description,'.');

        if($this->platform->title != 'Instagram'){
            $title = str_replace($this->hash_tag, $this->trim_hash, $this->news->title);
            $description = str_replace($this->hash_tag, $this->trim_hash, $this->shorten_point($this->news->description,'.'));
        }

        $title =  preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $title);
        $title = preg_replace('/\n(\s*\n){2,}/', "\n\n", $title);

        $description =  preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $description);
        $description = preg_replace('/\n(\s*\n){2,}/', "\n\n", $description);

        $shorten_url = $this->news->shorten_url;
        $full_creator = $this->source.$this->news->creator;
        if(!empty(strpos($this->news->creator,':')))
            $full_creator = $this->news->creator;

        $cre= $this->platform->title == 'Twitter'?($this->creator?$full_creator:null):$full_creator;


        $text = str_replace(array('[title]','[description]','[short_link]','[author]',),array($title,$description,$shorten_url,$cre),$temp['text']
        );

        if($this->platform->title == 'Facebook' or $this->platform->title == 'Twitter'){
            $text = str_replace('# ', '#', $text);
            $found = true;
            preg_match_all("/#([^\s]+)/", $text, $matches);
            if(isset($matches[0])){
                $matches[0] = array_reverse($matches[0]);
                $count = 0;
                foreach ($matches[0] as $hashtag){
                    if($count>=2)
                    {
                        $found = false;
                        $hashtag_without = str_replace('#','',$hashtag);
                        $hashtag_without = str_replace('_',' ',$hashtag_without);
                        $text = str_replace($hashtag,$hashtag_without,$text);

                    }
                    $count++;
                }

                if($count>=2) {
                    $found = false;
                }
            }

            if($found){
                $text = str_replace(array('[section]', '[sub_section]',), array($this->category, isset($this->sub_category->title)?$this->sub_category->title:null,), $text);
            }else{
                $text = str_replace(array('[section]', '[sub_section]', '|'), array('', '', '',), $text);
            }
        }elseif($this->platform->title=='Instagram'){
            $text = str_replace('# ', '#', $text);
            $text = str_replace(array('[section]', '[sub_section]', '|'), array('', '', ''), $text);
        }


        if(!empty($this->model->image)){
            $newsMedia = MediaNews::model()->findByAttributes(array('media'=>$this->model->image));
            if(!empty($newsMedia)){
                $media = $newsMedia->media;
            }
        }else{
            $newsMedia = MediaNews::model()->findAll('(type = "image" or type = "gallery") and news_id = '.$this->news->id);
            if(!empty($newsMedia)){
                $index_media= rand(0,count($newsMedia)-1);
                if(isset($newsMedia[$index_media])){
                    $media = $newsMedia[$index_media]->media;
                }
            }
        }
        if($this->platform->title == 'Twitter'){

            $newsMedia = MediaNews::model()->find('(type = "image") and news_id = '.$this->news->id);
            if(!empty($newsMedia)){
                $media = $newsMedia->media;
            }

            $text_twitter =$text;

            if($temp['type'] == 'Preview')
                if(!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!',$text,$matches))
                    $text =$text.PHP_EOL.$this->news->shorten_url;

            if($this->getTweetLength($text_twitter,$temp['type'] == 'Image'?true:false,$temp['type'] == 'Video'?true:false) > 141) {
                $is_scheduled = 0;
                $twitter_is_scheduled = true;
            }else{
                $twitter_is_scheduled =false;
                $text = $text_twitter.PHP_EOL;
                if($temp['type'] == 'Preview')
                    if(!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!',$text,$matches))
                        $text .=PHP_EOL.$this->news->shorten_url;
            }

        }else{
            if($twitter_is_scheduled){
                if(!$is_scheduled){
                    $is_scheduled = 1;
                }
            }
        }
        $text = str_replace('# #', '#', $text);
        $text = str_replace('##', '#', $text);
        $text = str_replace('&#8220;', '', $text);
        $text = str_replace('&#8221;', '', $text);
        $text = str_replace('&#8230;', '', $text);
        $text = str_replace('#8211;', '', $text);
        $text = str_replace('&#8211;', '', $text);
        $text = str_replace('&nbsp;', '', $text);
        $text = str_replace('&#160;', '', $text);
        $text = str_replace('&#8211;', '', $text);
        $text = str_replace('&ndash;', '', $text);
        if($this->platform->title != 'Instagram'){
            $text =  preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $text);
            $text = preg_replace('/\n(\s*\n){2,}/', "\n\n", $text);
        }

        $this->PostQueue = PostQueue::model()->findByPk($this->post_id);
        //$this->PostQueue->setIsNewRecord(true);
        //$this->PostQueue->id= null;
        $this->PostQueue->type = $temp['type'];
        $this->PostQueue->post = $text;
        //$this->PostQueue->schedule_date = $this->news->schedule_date;
        //$this->PostQueue->catgory_id =  $this->news->category_id;
        //$this->PostQueue->main_category_id =  $this->news->category_id;
        $this->PostQueue->link = $this->news->shorten_url;
        //$this->PostQueue->is_posted = 0;
        $this->PostQueue->news_id = $this->news->id;
        $this->PostQueue->post_id =null;
        $this->PostQueue->media_url =$media;
        //$this->PostQueue->settings ='general';
        //$this->PostQueue->is_scheduled =$is_scheduled;
        //$this->PostQueue->platform_id =$this->platform->id;
        //$this->PostQueue->generated ='auto';
        //$this->PostQueue->created_at =date('Y-m-d H:i:s');

        $this->PostQueue->save();



    }

}