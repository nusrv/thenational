<?php

/**
 * This is the model class for table "hashtag".
 *
 * The followings are the available columns in table 'hashtag':
 * @property integer $id
 * @property string $title
 * @property integer $category_id
 * @property integer $deleted
 * @property string $created_at
 *
 * The followings are the available model relations:
 * @property Category $category
 */
class Hashtag extends BaseModels
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'hashtag';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, created_at', 'required'),
			array('category_id, deleted', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			array('title','hashuse' ),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, category_id, deleted, created_at,pagination_size', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
		);
	}

	public function hashuse($attr){
		if (!preg_match('/[\'^#]/', $this->title))
		{
			return true;
		}
		$this->addError('title','you cannot insert a # simple');

		return false;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		$array= array(
			'id' => 'ID',
			'title' => 'Hashtag',
			'category_id' => 'Sections',
			'deleted' => 'Deleted',
			'created_at' => 'Created At',
			'visible_title' => 'Hashtag',
		);
		return array_merge($this->Labels(),$array);

	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		if(isset($this->pagination_size))
			$pages = $this->pagination_size;
		else
			$pages=5;
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('deleted',0);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'pagination' => array('pageSize' => $pages),
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Hashtag the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
