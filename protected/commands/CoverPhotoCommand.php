<?php
class CoverPhotoCommand extends BaseCommand{


    public function run($args){
        $this->TimeZone();
        $cover = $this->GetCoverPhotoFacebook();
        $this->Facebook($cover);
        $cover = $this->GetCoverPhotoTwitter();
        $this->Twitter($cover);
    }

    private function Facebook($cover){
        if(!empty($cover)){
            list($facebook,$PAGE_TOKEN)=$this->Load();
            $post=array(
                //'source' => '@' . 'cover.jpg',
                'no_story' => true // suppress automatic image upload story, optional
            );

            if(!empty($cover->media_url)){
                $content = file_get_contents($cover->media_url);
                file_put_contents(Yii::app()->params['webroot'].'/image/cover.jpg',$content);

                /*$post['source']='@'.Yii::app()->params['webroot'].'/image/cover.jpg';*/
            }
            //---------------------------
            $data = [
                'no_story' =>true,
                'source' =>$facebook->fileToUpload(Yii::app()->params['webroot'].'/image/cover.jpg'),
            ];

            try {
                $response = $facebook->post('/'.Yii::app()->params['facebook']['page_id'].'/photos', $data, $PAGE_TOKEN);
                $graphNode = $response->getGraphNode();
                $data = [
                    'cover' => $graphNode['id'],
                    'offset_x' => 0, // optional
                    'offset_y' => 0, // optional
                    'no_feed_story' => true // suppress automatic cover image story, optional
                ];
                $response = $facebook->post('/'.Yii::app()->params['facebook']['page_id'], $data, $PAGE_TOKEN);
                print_r($response);
                $cover->is_posted= 1;
                $cover->command= false;
                if(!$cover->save())
                    $this->send_email($cover,'error on facebook cover photo');

            } catch(Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                $cover->is_posted= 2;
                $cover->command= false;
                $this->send_email($cover,'error on facebook Graph cover photo');
                $cover->save();

            } catch(Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                $cover->is_posted= 2;
                $cover->command= false;
                $this->send_email($cover,'error on facebook SDK cover photo');
                $cover->save();
            }
        }

        return false;
    }

    private function Twitter($cover){

         if(!empty($cover)){

             $obj = $this->Obj_twitter();

             $params = array(
                 'width' => 1500,
                 'height' => 500,
                 'banner' => base64_encode(file_get_contents($cover->media_url)),
             );

             $reply = $obj->account_updateProfileBanner($params);

             $go =  isset($reply->errors);

             if($go){
                 $cover->is_posted= 2;
                 $cover->command= false;
                 if(!$cover->save())
                     $this->send_email($cover,'error on twitter cover photo');

                 return false;
             }

             $cover->is_posted= 1;
             $cover->command= false;

             if(!$cover->save())
                 $this->send_email($cover,'error on twitter cover photo');
             return true;

         }

        return false;

    }
    private function GetCoverPhotoFacebook(){

        return CoverPhoto::model()->get_cover_photo_facebook();
    }

    private function GetCoverPhotoTwitter(){

        return CoverPhoto::model()->get_cover_photo_twitter();
    }
}