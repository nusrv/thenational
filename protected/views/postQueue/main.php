<?php
/* @var $this PostQueueController */
/* @var $model PostQueue */
/* @var $facebook PostQueue */
/* @var $twitter PostQueue */
/* @var $instagram PostQueue */

$this->pageTitle = "";

Yii::app()->clientScript->registerCss('form-group', '
.form-inline .form-group {
    vertical-align: top;
    width: 100%;
}
.red{  color: #dd4b39;}
.green{
color:#00a65a;}
@media (min-width: 768px)
{.form-inline .form-control {
    display: inline-block;
    width: 100%;
    vertical-align: middle;
}
}
.my-video-dimensions {
    width: 100%;
    height: 264px;
}
.video-js .vjs-big-play-button {

    top: 40%;
    left: 40%;
    }
.panel-heading {
    padding: 10px 15px;
    border-bottom: 1px solid transparent;
    border-top-right-radius: 3px;
    border-top-left-radius: 3px;
    padding-bottom: 27px;
}
    video#my-video {
    width: 100%;
    height: 269px;
}
.editable-input {
    vertical-align: top;
    display: inherit;
    width: auto;
    white-space: normal;
    zoom: 1;
}
.editableform {
    margin-bottom: 0;
    background-color: #fff;
    padding: 10px;
    border-radius: 9px;
}
.editable-buttons {
    display: block;
    margin: 0 auto;
    text-align: center;
    margin-top: 16px;}
    /*.editable-click, a.editable-click, a.editable-click:hover {
    text-decoration: none;
    border-bottom: 0px;
    color: #444;
}*/
');
Yii::app()->clientScript->registerScript('search', "
$('#search-form').change(function(){
if($('#PostQueue_schedule_date').val() != '' ){
    $(this).submit();
    }
});
$('#PostQueue_schedule_date').attr('readonly','readonly');
$('#PostQueue_to').attr('readonly','readonly');
/*$('#yw28').contentChanged(function(){
});*/
");

$class = 'col-sm-4 small_height';
$counter = 1;
if (isset($Facebook)) {
    $counter++;
}
if (isset($Twitter)) {
    $counter++;
}
if (isset($Instagram)) {
    $counter++;
}

if ($counter == 2) {
    $class = 'col-sm-12 medium_height';
}

if ($counter == 3) {
    $class = 'col-sm-6 large_height';
}

?>
<style>
    .approve_post{
        cursor: pointer;
    }
</style>
<script>


    var timer = 5;

    window.App = {};
    $(window).load(function (){
        $('.approve_post').click(function(){

            var approve_id= $(this).data('id');





            $.post('<?PHP echo CController::createUrl('/postQueue/approvePosts/') ?>',{id:approve_id},function(data){


                if($('#check_approve_'+approve_id).hasClass('fa-square-o') && data=='Approved'){
                    $('#check_approve_'+approve_id).html('Approved');
                    $('#check_approve_'+approve_id).removeClass('fa-square-o').addClass('fa-check-square-o');
                }else{

                    $('#check_approve_'+approve_id).html(' ');
                    $('#check_approve_'+approve_id).addClass('fa-square-o').removeClass('fa-check-square-o');
                }
            });

        });
        App.sh =  function (id){
            $('#search-form').submit();
        };

        App.remove_post = function(id){

            var url = $(id).attr('data-url');
            BootstrapDialog.show({
                message: '<h4>Do you want to remove this post from all platforms?</h4>',
                buttons: [{
                    icon: 'fa fa-remove',
                    label: 'Remove from all platforms',
                    cssClass: 'btn-danger',
                    action: function(){
                        $.post(url,{all:1},function(data){
                            location.reload();
                        });
                    }
                }, {
                    icon: 'fa fa-remove',
                    label: 'Remove from this platform only ',
                    cssClass: 'btn-warning',
                    action: function(){
                        $.post(url,{all:0},function(data){
                            location.reload();
                        });
                    }
                }, {
                    label: 'cancel',
                    action: function(dialogItself){
                        dialogItself.close();
                    }
                }]
            });

        };
        $('.activate_platforms_generator').click(function(){

           var platform_activate = $(this).data('platform');
            var this_platform = $(this);
            $.post('<?PHP echo CController::createUrl('/postQueue/activate_platform/') ?>',{platform:platform_activate},function(data){

                if(data == 'success'){

                    if(this_platform.hasClass('fa-toggle-off')){
                        this_platform.removeClass('fa-toggle-off red');
                        this_platform.addClass('fa-toggle-on green');
                        this_platform.attr('data-original-title','status now on');
                        $('#'+this_platform.attr('data-platform')+'_col').css({ opacity: 1 });
                        location.reload();
                    }else{
                        this_platform.removeClass('fa-toggle-on green');
                        this_platform.addClass('fa-toggle-off red');
                        this_platform.attr('data-original-title','status now off');
                        $('#'+this_platform.attr('data-platform')+'_col').css({ opacity: 0.4 });
                    }
                }else{
                    alert('something went wrong please try again');
                }
            });
        });
        App.activate_post = function(id,platform_id,parent_id){
            var messages ='';
            var time_id = $('#time-'+id).val();
            var ids = '#activate_post'+id;
            var d = new Date(time_id);
            var x = new Date();
            $.post('<?PHP echo CController::createUrl('/postQueue/getPosts/') ?>',{id:id,platform_id:platform_id,parent_id:parent_id}, function( data ) {

                data = jQuery.parseJSON(data);

                if(x > d){
                    messages = '<h4>Do you want to add this post on all platforms?</h4><h5   class="alert alert-primary">Warning: if you publish this post it will be posted directly check posting time before continuing</h5>';

                }else if(!data['all']){
                    messages = '<h4>Do you want to add this post on all platforms?</h4><h5   class="alert alert-info">Warning(Add to all platforms): One of the related posts will be published directly check posting time before continuing</h5>';

                }
                else {
                    messages = '<h4>Do you want to add this post on all platforms?</h4>';

                }






                var url = $(ids).attr('data-url');

                BootstrapDialog.show({
                    message: messages,
                    buttons: [{
                        icon: 'fa fa-check',
                        label: 'Add to all platforms',
                        cssClass: 'btn-info',
                        action: function(){
                            $.post(url,{all:1},function(data){
                                location.reload();
                            });
                        }
                    }, {
                        icon: 'fa fa-check',
                        label: 'Add to this platform only ',
                        cssClass: 'btn-primary',
                        action: function(){
                            $.post(url,{all:0},function(data){
                                location.reload();
                            });
                        }
                    }, {
                        label: 'Cancel',
                        action: function(dialogItself){
                            dialogItself.close();
                        }
                    }]
                });
            });
        };


        App.push_post = function(id){
            var url = $(id).attr('data-url');
            BootstrapDialog.show({
                message: '<h4>Do you want to Push this post to all platforms?</h4>',
                buttons: [{
                    icon: 'fa fa-remove',
                    label: 'Push to all platforms',
                    cssClass: 'btn-danger',
                    action: function(){
                        $.post(url,{all:1},function(data){
                            location.reload();
                        });
                    }
                }, {
                    icon: 'fa fa-remove',
                    label: 'Push to this platform only ',
                    cssClass: 'btn-warning',
                    action: function(){
                        $.post(url,{all:0},function(data){
                            location.reload();
                        });
                    }
                }, {
                    label: 'cancel',
                    action: function(dialogItself){
                        dialogItself.close();
                    }
                }]
            });
        };


        App.re_post = function(id){
            var url = $(id).attr('data-url');
            BootstrapDialog.show(
            {
                message: '<h4>Do you want to repush this post ?</h4>',
                buttons: [     {
                    icon: 'fa fa-remove',
                    label: 'Repush to all platforms',
                    cssClass: 'btn-danger',
                    action: function(){
                        $.post(url,{all:1},function(data){
                            location.reload();
                        });
                    }
                },{
                    icon: 'fa fa-remove',
                    label: 'Repush to this platform only',
                    cssClass: 'btn-warning',
                    action: function(){
                        $.post(url,{all:0},function(data){
                            location.reload();
                        });
                    }
                }, {
                    label: 'cancel',
                    action: function(dialogItself){
                        dialogItself.close();
                    }
                }]
            });
        };
        App.edit_pined = function(id,pinned){


            $.post('<?PHP echo CController::createUrl('/postQueue/edit_pined/') ?>',{id:id}, function( data ) {

                if(data==1) {
                    $('#img_pinned_'+id).attr('src','<?php echo Yii::app()->baseUrl.'/image/note2.png'; ?>');
                    $('#img_pinned_'+id).removeAttr('title');
                    $('#img_pinned_'+id).removeAttr('data-original-title');
                    $('#img_pinned_'+id).attr('data-original-title','Unpin');
                }else{
                    $('#img_pinned_'+id).attr('src','<?php echo Yii::app()->baseUrl.'/image/pin_black.png'; ?>');
                    $('#img_pinned_'+id).removeProp('title');
                    $('#img_pinned_'+id).removeAttr('data-original-title');
                    $('#img_pinned_'+id).attr('data-original-title','Pin');

                }



            });


        };


    });

</script>

<section class="content">
    <div class="row">

        <div class="col-sm-12" style="margin-bottom: 10px;">
                    <?php echo CHtml::link('Quick create',CController::CreateUrl('postQueue/create'),array('class'=>'btn btn-info '))?>
        </div>

        <div class="col-sm-12">
            <div class="box">
                <div class="box-header with-border">

                    <?php $this->renderPartial('_adv_search', array('model' => $model,'this',$this)); ?>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="box">
                <div class="box-body post_queue_main">

                    <?php
                    if(Yii::app()->controller->action->id == 'main'){
                        $emptymsg = '<div class="col-sm-12 font-open-sans">There are no scheduled posts to be shown</div>';
                    }elseif(Yii::app()->controller->action->id == 'mainUn'){
                        $emptymsg = '<div class="col-sm-12 font-open-sans">There are no unscheduled posts to be shown</div>';

                    }elseif(Yii::app()->controller->action->id == 'mainPosted'){
                        $emptymsg = '<div class="col-sm-12 font-open-sans">There is no posts that were posted</div>';

                    }elseif(Yii::app()->controller->action->id == 'mainPinned'){
                        $emptymsg = '<div class="col-sm-12 font-open-sans">There is no pinned posts</div>';

                    }
                    ?>

                    <?PHP if (isset($Facebook)) {

                        $check_platform_active_facebook = Platform::model()->findByAttributes(array('title'=>'Facebook'));
                        ?>
                        <div class="<?PHP echo $class ?>">
                            <ul class="timeline">
                                <li class="time-label">
                  <span class="bg-blue">Facebook</span>
                                    <span class="pull-right">
                                        <span>Status:</span>
                                        <?php

                                            if ($check_platform_active_facebook->active == 0) {
                                                ?>
                                                <i class="fa fa-toggle-off activate_platforms_generator red"
                                                   style="font-size:20px;cursor: pointer;  "
                                                   data-platform="Facebook" title="status now off" data-toggle="tooltip"></i>
                                                <?php
                                            } else {

                                                ?>
                                                <i class="fa fa-toggle-on activate_platforms_generator green "
                                                   style="font-size:20px;cursor: pointer;"
                                                   data-platform="Facebook" title="status now on" data-toggle="tooltip"></i>
                                                <?php
                                            }

                                        ?>
                                    </span>
                                    <div  id="Facebook_col" style="<?= $check_platform_active_facebook->active == 1?'':'opacity: .4;'?>">
                                    <?PHP
                                    $this->widget(
                                        'booster.widgets.TbThumbnails',
                                        array(
                                            'dataProvider' => $Facebook,
                                            'template' => "{items}\n{pager}",
                                            'itemView' => '_view_post',
                                            'emptyText' => $emptymsg,
                                            'ajaxUpdate'=>true,
                                            'afterAjaxUpdate'=>'js:console.log("test")',
                                            'updateSelector'=>'custom-page-selector', //update selector


                                            'pager' => array(
                                                'class' => 'booster.widgets.TbPager',
                                                'maxButtonCount' => 4,
                                                'nextPageLabel' => 'Next',
                                                'prevPageLabel' => 'Previous',
                                                'firstPageLabel' => 'First',
                                                'lastPageLabel' => 'Last',
                                                'htmlOptions' => array(
                                                    'class' => 'pull-left'
                                                )
                                            ),
                                        )
                                    );
                                    ?>
                                    </div>
                                </li>
                            </ul>

                        </div>
                    <?PHP } ?>
                    <?PHP if (isset($Twitter)) { ?>
                        <div class="<?PHP echo $class ?>">

                            <ul class="timeline">
                                <li class="time-label">
                  <span class="bg-aqua" style="padding-left:15px;padding-right:15px ;">
                     Twitter

                  </span>
                                    <span class="pull-right">
                                        Status:
                                        <?php
                                        $check_platform_active_twitter = Platform::model()->findByAttributes(array('title'=>'Twitter'));

                                        if($check_platform_active_twitter->active ==0) {
                                            ?>
                                            <i class="fa fa-toggle-off activate_platforms_generator red" style="font-size:20px;cursor: pointer;"
                                               data-platform="Twitter" title="status now off" data-toggle="tooltip"></i>
                                            <?php
                                        }else{

                                            ?>
                                            <i class="fa fa-toggle-on activate_platforms_generator green" style="font-size:20px;cursor: pointer;"
                                               data-platform="Twitter" title="status now on" data-toggle="tooltip"></i>
                                        <?php
                                        }
                                        ?>
                                    </span>
                                    <div id="Twitter_col" style="<?= $check_platform_active_twitter->active == 1?'':'opacity: .4;'?>">
                                    <?PHP
                                    $this->widget(
                                        'booster.widgets.TbThumbnails',
                                        array(
                                            'dataProvider' => $Twitter,
                                            'template' => "{items}\n{pager}",
                                            'itemView' => '_view_post',
                                            'emptyText' => $emptymsg,
                                            'ajaxUpdate'=>true,
                                            'afterAjaxUpdate'=>'js:console.log("test")',
                                            'updateSelector'=>'custom-page-selector', //update selector

                                            'pager' => array(
                                                'class' => 'booster.widgets.TbPager',
                                                'maxButtonCount' => 4,
                                                'nextPageLabel' => 'Next',
                                                'prevPageLabel' => 'Previous',
                                                'firstPageLabel' => 'First',
                                                'lastPageLabel' => 'Last',
                                                'htmlOptions' => array(
                                                    'class' => 'pull-left'
                                                )
                                            ),
                                        )
                                    );
                                    ?>
                                        </div>
                                </li>
                            </ul>

                        </div>
                    <?PHP } ?>

                    <?PHP if (isset($Instagram)) { ?>
                        <div class="<?PHP echo $class ?>">
                            <ul class="timeline">
                                <li class="time-label">
                  <span class="bg-purple">
                    Instagram
                  </span>
                                    <span class="pull-right">
                                        Status:
                                        <?php
                                        $check_platform_active_instagram = Platform::model()->findByAttributes(array('title'=>'Instagram'));

                                        if($check_platform_active_instagram->active ==0) {
                                            ?>
                                            <i class="fa fa-toggle-off activate_platforms_generator red" style="font-size:20px;cursor: pointer;"
                                               data-platform="Instagram" title="status now off" data-toggle="tooltip"></i>
                                            <?php
                                        }else{

                                            ?>
                                            <i class="fa fa-toggle-on activate_platforms_generator green" style="font-size:20px;cursor: pointer;"
                                               data-platform="Instagram" title="status now on" data-toggle="tooltip"></i>
                                            <?php
                                        }
                                        ?>
                                    </span>
                                    <div id="Instagram_col" style="<?= $check_platform_active_instagram->active == 1?'':'opacity: .4;'?>">
                                    <?PHP
                                    $this->widget(
                                        'booster.widgets.TbThumbnails',
                                        array(
                                            'dataProvider' => $Instagram,
                                            'template' => "{items}\n{pager}",
                                            'itemView' => '_view_post',
                                            'emptyText' => $emptymsg,
                                            'ajaxUpdate'=>true,
                                            'afterAjaxUpdate'=>'js:console.log("test")',

                                            'updateSelector'=>'custom-page-selector', //update selector

                                            'pager' => array(
                                                'class' => 'booster.widgets.TbPager',
                                                'maxButtonCount' => 4,
                                                'nextPageLabel' => 'Next',
                                                'prevPageLabel' => 'Previous',
                                                'firstPageLabel' => 'First',
                                                'lastPageLabel' => 'Last',
                                                'htmlOptions' => array(
                                                    'class' => 'pull-left'
                                                )
                                            ),
                                        )
                                    );
                                    ?>
                                        </div>
                                </li>
                            </ul>
                        </div>
                    <?PHP } ?>
                </div>

            </div>

        </div>
</section>

<?php //'confirm'=>'Do you want to delete all posts about this program from this calendar?'
//Yii::app()->createUrl("site/delete/".$data->id) ?>
