<?php
/* @var $this PostQueueController */
/* @var $model PostQueue */
/* @var $criteria PostQueue */
$this->pageTitle = "View";

$this->breadcrumbs=array('Post Queues'=>array('main'),$model->id);
?>
<style>
    .verticalLine {
        border-left: thick solid #ff0000;
    }

</style>
<script>

    window.App = {};
    $(window).load(function (){
        App.sh =  function (id){
            $('#search-form').submit();
        };
        App.remove_post = function(id){
            var url = $(id).attr('data-url');
            BootstrapDialog.show({
                message: '<h4>Do you want to remove this post from all platforms?</h4>',
                buttons: [{
                    icon: 'fa fa-remove',
                    label: 'Remove from all platforms',
                    cssClass: 'btn-danger',
                    action: function(){
                        $.post(url,{all:1},function(data){
                            location.reload();
                        });
                    }
                }, {
                    icon: 'fa fa-remove',
                    label: 'Remove from this platform only ',
                    cssClass: 'btn-warning',
                    action: function(){
                        $.post(url,{all:0},function(data){
                            location.reload();
                        });
                    }
                }, {
                    label: 'cancel',
                    action: function(dialogItself){
                        dialogItself.close();
                    }
                }]
            });
        };
        App.activate_post = function(id,platform_id,parent_id){
            var messages ='';
            var time_id = $('#time-'+id).val();
            var ids = '#activate_post'+id;
            var d = new Date(time_id);
            var x = new Date();

            $.post('<?PHP echo CController::createUrl('/postQueue/getPosts/') ?>',{id:id,platform_id:platform_id,parent_id:parent_id}, function( data ) {

                data = jQuery.parseJSON(data);

                if(x > d){
                    messages = '<h4>Do you want to add this post on all platforms?</h4><h5   class="alert alert-primary">Warning: if you publish this post it will be posted directly check posting time before continuing</h5>';

                }else if(!data['all']){
                    messages = '<h4>Do you want to add this post on all platforms?</h4><h5   class="alert alert-info">Warning(Add to all platforms): One of the related posts will be published directly check posting time before continuing</h5>';

                }
                else {
                    messages = '<h4>Do you want to add this post on all platforms?</h4>';

                }






                var url = $(ids).attr('data-url');

                BootstrapDialog.show({
                    message: messages,
                    buttons: [{
                        icon: 'fa fa-check',
                        label: 'Add to all platforms',
                        cssClass: 'btn-info',
                        action: function(){
                            $.post(url,{all:1},function(data){
                                location.reload();
                            });
                        }
                    }, {
                        icon: 'fa fa-check',
                        label: 'Add to this platform only ',
                        cssClass: 'btn-primary',
                        action: function(){
                            $.post(url,{all:0},function(data){
                                location.reload();
                            });
                        }
                    }, {
                        label: 'Cancel',
                        action: function(dialogItself){
                            dialogItself.close();
                        }
                    }]
                });
            });
        };

        App.select_image = function(news_id,platforms,media){
            var id=null;
            if(platforms == 1) {
                var plat = 'Facebook';
                 id = $('.Facebook_hidden_inputs').attr('id');
            }
            else if(platforms == 2) {
                plat = 'Twitter';
                 id = $('.twitter_hidden_inputs').attr('id');
            }
            else if(platforms == 3) {
                plat = 'Instagram';
                 id = $('.Instagram_hidden_inputs').attr('id');

            }
            else {
                id = '<?php echo $model->id; ?>';
                plat = 'All Platforms';
            }
            var result = confirm("Do you want to change this image for "+plat+"?");
            if(result) {
                $.post('<?PHP echo CController::createUrl('/postQueue/changeImage/') ?>', {
                    id:id,
                    news_id: news_id,
                    platforms: platforms,
                    media: media
                }, function (data) {
                    if(platforms==0){
                        $('.chngerimg').attr('src',media);
                    }else
                        $('#changer'+data).attr('src',media);


                    /*
                     alert(data);
                     */
                    /*location.reload();*/
                });
            }
        }
        App.push_post = function(id){
            var url = $(id).attr('data-url');
            BootstrapDialog.show({
                message: '<h4>Do you want to Push this post to all platforms?</h4>',
                buttons: [{
                    icon: 'fa fa-remove',
                    label: 'Push to all platforms',
                    cssClass: 'btn-danger',
                    action: function(){
                        $.post(url,{all:1},function(data){
                            location.reload();
                        });
                    }
                }, {
                    icon: 'fa fa-remove',
                    label: 'Push to this platform only ',
                    cssClass: 'btn-warning',
                    action: function(){
                        $.post(url,{all:0},function(data){
                            location.reload();
                        });
                    }
                }, {
                    label: 'cancel',
                    action: function(dialogItself){
                        dialogItself.close();
                    }
                }]
            });
        };
        App.re_post = function(id){
            var url = $(id).attr('data-url');
            BootstrapDialog.show({
                message: '<h4>Do you want to re post this post to all platforms?</h4>',
                buttons: [{
                    icon: 'fa fa-remove',
                    label: 're post to all platforms',
                    cssClass: 'btn-danger',
                    action: function(){
                        $.post(url,{all:1},function(data){
                            location.reload();
                        });
                    }
                }, {
                    icon: 'fa fa-remove',
                    label: 're post to this platform only ',
                    cssClass: 'btn-warning',
                    action: function(){
                        $.post(url,{all:0},function(data){
                            location.reload();
                        });
                    }
                }, {
                    label: 'cancel',
                    action: function(dialogItself){
                        dialogItself.close();
                    }
                }]
            });
        };



        $('.readmore').each(function(key,val){
            if($(this).html().length > 300){
                var results = $(this).html();
                var text = $(this).html().substr(0,200) + '...';
                var result = text.replace('...', '<p class="readmores">Read more</p>');
                $(this).html(result);
            }
            $('.readmores').click(function(){
                $(this).parent().html(results)/*.append('<p class="readless">Read Less</p>')*/;
            });
            $('.readless').click(function(){
                $(this).parent().html(result);
            });
        });
        $(".textarea_edit").keyup(function(){
            $(this).parent().parent().find('button').show();

        })
        function getSelectedText(el) {
            if (typeof el.selectionStart == "number") {
                return el.value.slice(el.selectionStart, el.selectionEnd);
            } else if (typeof document.selection != "undefined") {
                var range = document.selection.createRange();
                if (range.parentElement() == el) {
                    return range.text;
                }
            }
            return "";
        }


        var copyBtn = document.querySelector('#input');
        $('.tooltips').hide();


        copyBtn.addEventListener('click', function () {
            var urlField = document.querySelector('.textarea_edit');

            try {
                if(document.execCommand('copy')) {



                }
            } catch (err) {
                console.log('Oops, unable to copy');
            }
            // create a Range object
            /*var range = document.createRange();
             // set the Node to select the "range"
             range.selectNode(urlField);
             // add the Range to the set of window selections
             window.getSelection().addRange(range);*/

            // execute 'copy', can't 'cut' in this case

            document.execCommand('copy');
        }, false);



        $('.textarea_edit').mouseup(function(){



            var highlight = window.getSelection();

            var text = $(this).val();


            if(highlight !=0) {

                if(getSelectedText(copyBtn) !="") {

                    $(this).val(text.replace(highlight, getSelectedText(copyBtn)));
                    $(this).parent().parent().find('button').show();

                }
            }



        })
        $('#input').mouseup(function(){
            var highlights = window.getSelection();
            if(highlights !=0) {
                $('.tooltips').show();

                setInterval(function () {
                    $('.tooltips').hide();
                }, 3000);
            }

        })
    });

</script>
<style>
    .fa-facebook{

        color:#3b5998;
    }
    .fa-twitter {
        color:#00aced;
    }
    .fa-instagram{
        color: #517FA6;
    }
    .fa-reply-all{
        color:#424242;
    }
    .iconss{
        margin-top:10px;
        font-size:16px;
        cursor: pointer;
        margin-left:15px;
    }
    .readmores{
        color:blue;
        cursor: pointer;
    }
    .readless{
        color:blue;
        cursor: pointer;
    }
</style>
<section class="content">
    <?PHP if(Yii::app()->user->hasFlash('success')){ ?>
        <div class="callout callout-success" style="margin-top:20px;">
            <h4>Created successfully. </h4>
         </div>

    <?PHP } if(Yii::app()->user->hasFlash('update')){ ?>
        <div class="callout callout-info" style="margin-top:20px;">
            <h4>Updated successfully. </h4>
        </div>
   <?php } ?>
	<div class="row">
        <div class="col-sm-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="col-sm-12 col-sm-push-8"><?php  $this->widget(
                            'booster.widgets.TbButtonGroup',
                            array(
                                'size' => 'small',
                                'context' => 'info',
                                'buttons' => array(
                                    array(
                                        'label' => 'Post queue',
                                        'context'=>'success',
                                        'buttonType' =>'link',
                                        'url' => array('/postQueue/main'),

                                    ),
                                    array(
                                        'label' => 'Unscheduled posts',
                                        'buttonType' =>'link',
                                        'context'=>'primary',
                                        'url' => array('/postQueue/mainUn'),
                                        'htmlOptions' => array('class' => 'btns-scheduledposts'), // for inset effect

                                    ),
                                ),
                            )
                        ); ?></div>                </div>
                <div class="box-body">
                    <div class="row">
                        <?PHP $this->renderPartial('_post',array('model'=>$model)) ?>
                    </div>
                    <div class="row">
                        <hr>
                            <?php
                            $mod = $model->GetPost($model->id,$model->platform_id, $model->parent_id);
                            if(!empty($mod)){
                            echo "<div class='col-sm-12'><h2 style='color:darkred;text-align: center;'>Related Post</h2></div>";

                            foreach($mod  as $items) { ?>
                                <div class="col-sm-6">
                                    <?php $this->renderPartial('_post',array('model'=>$items));?>
                                </div>
                            <?php } } ?>

                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#relatedImages">Related images</a></li>
                            <li><a data-toggle="tab" href="#relatedtext">Related text</a></li>
                            </ul>
                            <?php
                        if((isset($model->type) == 'Image') or (isset($items->type) == 'Image') or(isset($model->type) =='Preview' or isset($items->type) == 'Preview')){
                            echo '<div class="tab-content">';

                            echo '<div class="col-sm-12 tab-pane fade in active">';
                            MediaNews::model()->get_related_images($model);

                        $this->renderPartial('_upload_image',array('model'=>$model));
                            echo '</div>';


                        }
                            echo '<div class="col-sm-12 tab-pane fade" id="relatedtext">';
                            News::model()->get_related_text_update($model,true);
                            echo "</div>";
                        ?>

                    </div>
                </div>
            </div>
        </div>

</div></section>

