<?php

class m161204_154720_Categories_level extends CDbMigration
{
	public function up()
	{
	    $this->execute('
Delete From `category_level`;
INSERT INTO `category_level` (`id`, `source_id`, `predication`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(2, 61, \'div[class=editors-picks] ul li div[class=artbox-right]  a\', \'2016-12-04 14:55:07\', NULL, 1, NULL),
(3, 61, \'div[class=con-mid-frontpage] div[class=mainflash-frontpage] div[class=mainflash-body]  h4[class=maintab-headline] a\', \'2016-12-04 15:12:37\', NULL, 1, NULL),
(4, 61, \'div[class=containermid-last] div[class=holder]  div[class=conleft]  div[class=list1]  ul li div h4 a\', \'2016-12-04 15:37:50\', NULL, 1, NULL),
(5, 62, \'div[class=containermid-first] div[class=holder]  div[class=list1-full] ul li div h4 a\', \'2016-12-04 15:41:16\', NULL, 1, NULL);');
	}

	public function down()
	{
		echo "m161204_154720_Categories_level does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}