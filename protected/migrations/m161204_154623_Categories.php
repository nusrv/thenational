<?php

class m161204_154623_Categories extends CDbMigration
{
	public function up()
	{
	    $this->execute('
Delete From `category`;
INSERT INTO `category` (`id`, `title`, `lang`, `url`, `deleted`, `active`, `type`, `url_rss`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, \'Courts\', \'en\', \'http://www.thenational.ae/uae/courts\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1008\', \'2016-11-09 15:37:40\', \'2016-12-04 17:32:54\', NULL, 1),
(2, \'Education\', \'en\', \'http://www.thenational.ae/uae/education\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1009\', \'2016-11-09 15:37:40\', \'2016-12-04 17:33:05\', NULL, 1),
(3, \'Environment\', \'en\', \'http://www.thenational.ae/uae/environment\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1010\', \'2016-11-09 15:37:40\', \'2016-12-04 17:33:06\', NULL, 1),
(4, \'Government\', \'en\', \'http://www.thenational.ae/uae/government\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1013\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(5, \'Health\', \'en\', \'http://www.thenational.ae/uae/health\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1011\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(6, \'Heritage\', \'en\', \'http://www.thenational.ae/uae/heritage\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1012\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(7, \'Science\', \'en\', \'http://www.thenational.ae/uae/science\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1014\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(8, \'Technology\', \'en\', \'http://www.thenational.ae/uae/technology\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1015\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(9, \'Tourism\', \'en\', \'http://www.thenational.ae/uae/tourism\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1016\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(10, \'Transport\', \'en\', \'http://www.thenational.ae/uae/transport\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1017\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(11, \'Middle East and North Africa\', \'en\', \'http://www.thenational.ae/world/middle east and north africa\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1018\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(12, \'South Asia\', \'en\', \'http://www.thenational.ae/world/south asia\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1019\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(13, \'Americas\', \'en\', \'http://www.thenational.ae/world/americas\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1020\', \'2016-11-09 15:37:41\', \'2016-12-04 17:32:47\', NULL, 1),
(14, \'Europe\', \'en\', \'http://www.thenational.ae/world/europe\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1021\', \'2016-11-09 15:37:41\', \'2016-12-04 17:33:07\', NULL, 1),
(15, \'Asia Pacific\', \'en\', \'http://www.thenational.ae/world/asia pacific\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1022\', \'2016-11-09 15:37:41\', \'2016-12-04 17:32:49\', NULL, 1),
(16, \'Africa\', \'en\', \'http://www.thenational.ae/world/africa\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1023\', \'2016-11-09 15:37:41\', \'2016-12-04 17:32:47\', NULL, 1),
(17, \'Central Asia\', \'en\', \'http://www.thenational.ae/world/central asia\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1024\', \'2016-11-09 15:37:41\', \'2016-12-04 17:32:52\', NULL, 1),
(18, \'East Asia\', \'en\', \'http://www.thenational.ae/world/east asia\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1025\', \'2016-11-09 15:37:41\', \'2016-12-04 17:32:56\', NULL, 1),
(19, \'Southeast Asia\', \'en\', \'http://www.thenational.ae/world/southeast asia\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1026\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(20, \'Personal Finance\', \'en\', \'http://www.thenational.ae/business/personal finance\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1034\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(21, \'Aviation\', \'en\', \'http://www.thenational.ae/business/aviation\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1027\', \'2016-11-09 15:37:41\', \'2016-12-04 17:32:49\', NULL, 1),
(22, \'Banking\', \'en\', \'http://www.thenational.ae/business/banking\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1028\', \'2016-11-09 15:37:42\', \'2016-12-04 17:32:50\', NULL, 1),
(23, \'Economy\', \'en\', \'http://www.thenational.ae/business/economy\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1029\', \'2016-11-09 15:37:42\', \'2016-12-04 17:32:59\', NULL, 1),
(24, \'Energy\', \'en\', \'http://www.thenational.ae/business/energy\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1030\', \'2016-11-09 15:37:42\', \'2016-12-04 17:33:06\', NULL, 1),
(25, \'The Life\', \'en\', \'http://www.thenational.ae/business/the life\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1719\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(26, \'Markets\', \'en\', \'http://www.thenational.ae/business/markets\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1032\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(27, \'Media\', \'en\', \'http://www.thenational.ae/business/media\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1033\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(28, \'Property\', \'en\', \'http://www.thenational.ae/business/property\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1035\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(29, \'Retail\', \'en\', \'http://www.thenational.ae/business/retail\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1036\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(30, \'Technology\', \'en\', \'http://www.thenational.ae/business/technology\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1037\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(31, \'Telecoms\', \'en\', \'http://www.thenational.ae/business/telecoms\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1038\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(32, \'Travel & Tourism\', \'en\', \'http://www.thenational.ae/business/travel & tourism\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1039\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(33, \'UAE Sport\', \'en\', \'http://www.thenational.ae/sport/uae sport\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1051\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(34, \'Football\', \'en\', \'http://www.thenational.ae/sport/football\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1042\', \'2016-11-09 15:37:42\', \'2016-12-04 17:33:14\', NULL, 1),
(35, \'Motorsport\', \'en\', \'http://www.thenational.ae/sport/motorsport\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1046\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(36, \'Cricket\', \'en\', \'http://www.thenational.ae/sport/cricket\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1041\', \'2016-11-09 15:37:42\', \'2016-12-04 17:32:55\', NULL, 1),
(37, \'Golf\', \'en\', \'http://www.thenational.ae/sport/golf\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1044\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(38, \'Horse Racing\', \'en\', \'http://www.thenational.ae/sport/horse racing\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1045\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(39, \'Tennis\', \'en\', \'http://www.thenational.ae/sport/tennis\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1050\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(40, \'Rugby\', \'en\', \'http://www.thenational.ae/sport/rugby\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1049\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(41, \'North American Sport\', \'en\', \'http://www.thenational.ae/sport/north american sport\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1047\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(42, \'Other Sport\', \'en\', \'http://www.thenational.ae/sport/other sport\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1048\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(43, \'Comment-Sport\', \'en\', \'http://www.thenational.ae/sport/comment-sport\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/2651\', \'2016-11-09 15:37:43\', \'2016-12-04 17:32:54\', NULL, 1),
(44, \'Film\', \'en\', \'http://www.thenational.ae/arts & life/film\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1061\', \'2016-11-09 15:37:43\', \'2016-12-04 17:33:12\', NULL, 1),
(45, \'Art\', \'en\', \'http://www.thenational.ae/arts & life/art\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1059\', \'2016-11-09 15:37:43\', \'2016-12-04 17:32:48\', NULL, 1),
(46, \'Books\', \'en\', \'http://www.thenational.ae/arts & life/books\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1060\', \'2016-11-09 15:37:43\', \'2016-12-04 17:32:51\', NULL, 1),
(47, \'Music\', \'en\', \'http://www.thenational.ae/arts & life/music\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1062\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(48, \'On Stage\', \'en\', \'http://www.thenational.ae/arts & life/on stage\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1063\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(49, \'Television\', \'en\', \'http://www.thenational.ae/arts & life/television\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1064\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(50, \'Food\', \'en\', \'http://www.thenational.ae/arts & life/food\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1054\', \'2016-11-09 15:37:43\', \'2016-12-04 17:33:12\', NULL, 1),
(51, \'Fashion\', \'en\', \'http://www.thenational.ae/arts & life/fashion\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1053\', \'2016-11-09 15:37:43\', \'2016-12-04 17:33:10\', NULL, 1),
(52, \'Travel\', \'en\', \'http://www.thenational.ae/arts & life/travel\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1057\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(53, \'Well Being\', \'en\', \'http://www.thenational.ae/arts & life/well being\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1058\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(54, \'Family\', \'en\', \'http://www.thenational.ae/arts & life/family\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1052\', \'2016-11-09 15:37:43\', \'2016-12-04 17:33:10\', NULL, 1),
(55, \'Home & Garden\', \'en\', \'http://www.thenational.ae/arts & life/home & garden\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1055\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(56, \'Motoring\', \'en\', \'http://www.thenational.ae/arts & life/motoring\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/1056\', \'2016-12-04 15:33:28\', NULL, NULL, NULL),
(57, \'Comment-A&L\', \'en\', \'http://www.thenational.ae/arts & life/comment-a&l\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/2652\', \'2016-11-09 15:37:43\', \'2016-12-04 17:32:53\', NULL, 1),
(58, \'Comment\', \'en\', \'http://www.thenational.ae/opinion/comment\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/2341\', \'2016-11-09 15:37:43\', \'2016-12-04 17:32:53\', NULL, 1),
(59, \'Editorial\', \'en\', \'http://www.thenational.ae/opinion/editorial\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/2342\', \'2016-11-09 15:37:43\', \'2016-12-04 17:32:57\', NULL, 1),
(60, \'Feedback\', \'en\', \'http://www.thenational.ae/opinion/feedback\', 0, 0, \'rss\', \'http://www.thenational.ae/section/rss/2343\', \'2016-11-09 15:37:43\', \'2016-12-04 17:33:11\', NULL, 1),
(61, \'Main Page\', \'en\', \'http://www.thenational.ae/\', 0, 1, \'dom\', \'\', \'2016-12-04 15:33:28\', \'2016-12-04 17:33:40\', 1, 1),
(62, \'Review\', \'en\', \'http://www.thenational.ae/arts-life/the-review\', 0, 1, \'dom\', \'\', \'2016-12-04 15:33:28\', \'2016-12-04 17:33:49\', 1, 1);');
	}

	public function down()
	{
		echo "m161204_154623_Categories does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}