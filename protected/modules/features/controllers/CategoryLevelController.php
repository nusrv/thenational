<?php

class CategoryLevelController extends BaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','test'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
    public function actionTest()
    {
        if(isset($_POST['predication'])) {
            $items_predications = $_POST['predication'];
            $items_sources = $_POST['Sources'];

            $html_dom = new SimpleHTMLDOM();
            $counter = 0;

            $model_source = Category::model()->findByPk($items_sources);

            $html_dom_url = $html_dom->file_get_html($model_source->url);

            if (isset($html_dom_url)) {
                if(strpos($items_predications,'div[class=containermid-last] ') !== false){
                    $words=explode(' ', $items_predications);
                    array_shift($words);
                    $items_predications =  implode(' ', $words);
                $html_content = $html_dom_url->find('div[class=containermid-last] ',0)->find($items_predications);
                }else{
                    $html_content = $html_dom_url->find($items_predications);
                }
                if (isset($html_content) and !empty($html_content)) {
                    $news_counter = 0;
                    echo '<div class="col-xs-4" style="overflow: auto;"><h3 class="alert-success">success predication : <b>' . $model_source->title . '</b></h3>';
                    foreach ($html_content as $contents) {
                        $news_counter++;
                        if ($news_counter < 6) {
                            if (strpos($contents->href, 'http') === 0) {

                                echo '<h5><a href="' . $contents->href . '" target="_blank">' . $contents->href . '</a></h5>';
                            } else {
                                echo '<h5><a href="' . Yii::app()->params['feedUrl'] . $contents->href . '" target="_blank">' . Yii::app()->params['feedUrl'] . $contents->href . '</a></h5>';

                            }
                        }

                    }
                    echo '<h5 style="color:darkred">Data total : ' . $news_counter . '</h5>';
                    echo '</div>';
                } else {
                    $data['invalid_urls'][$counter] = $model_source;
                    $counter++;
                }

                echo '<div class="col-xs-4">';
                if (isset($data['invalid_urls'])) {
                    foreach ($data['invalid_urls'] as $invlid_urls) {
                        echo '<h3 class="alert-danger" data-value="' . $invlid_urls->id . '">Invalid Predication :  <b>' . $invlid_urls->title . '</b></h3>';
                    }
                    echo '</div>';
                }
            }
        }

    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new CategoryLevel;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CategoryLevel']))
		{
			$model->attributes=$_POST['CategoryLevel'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        $models=new CategoryLevel('search');
        $models->unsetAttributes();  // clear any default values
        if(isset($_GET['CategoryLevel']))
            $models->attributes=$_GET['CategoryLevel'];

		// Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

		if(isset($_POST['CategoryLevel']))
		{
			$model->attributes=$_POST['CategoryLevel'];
			if($model->save())
				$this->redirect(array('index'));
		}

		$this->render('index',array(
			'model'=>$model,
            'models'=>$models
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{

        $model=new CategoryLevel;
        $models=new CategoryLevel('search');
        $models->unsetAttributes();  // clear any default values
        if(isset($_GET['CategoryLevel']))
            $models->attributes=$_GET['CategoryLevel'];

        $this->performAjaxValidation($model);
        if(isset($_POST['CategoryLevel']))
        {
            if(isset($_POST['CategoryLevel']['source_id'])){
            $source = $_POST['CategoryLevel']['source_id'];
            foreach($source as $sources) {
                $model->attributes = $_POST['CategoryLevel'];
                $model->id=null;
                $model->source_id = $sources;
                $model->created_at = date('Y-m-d H:i:s');
                $model->setIsNewRecord(true);
                $model->save();
            }
            }
                $this->redirect(array('index'));
        }

        $this->render('index',array(
            'model'=>$model,
            'models'=>$models
        ));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new CategoryLevel('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['CategoryLevel']))
			$model->attributes=$_GET['CategoryLevel'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return CategoryLevel the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=CategoryLevel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CategoryLevel $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='category-level-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
