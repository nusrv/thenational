<?php
class TestDomController extends FeaturesBaseController{
    public function actionIndex(){
        $html = new SimpleHTMLDOM();
        $news = News::model()->findAll();

        $categories = Category::model()->findAll('type="dom"');
        $counter=0;
        $data=array();
        $replaced=null;
        if(!empty($categories)) {
            foreach ($categories as $category) {
                $html_url = $html->file_get_html($category->url);
                if (isset($html_url)) {
                    $categories_predication = CategoryLevel::model()->findAllByAttributes(array('source_id' => 1));
                    foreach ($categories_predication as $cat_predict) {
                        $html_content = $html_url->find($cat_predict->predication);
                        if(isset($html_content)) {
                            foreach ($html_content as $content) {
                                if($counter <10) {
                                    if (strpos($content->href, 'http') === 0) {
                                        $data['url'][$counter] = $content->href;
                                    } else
                                        $data['url'][$counter] = $content->href;


                                    list($title, $description, $body_description, $video, $image_og, $gallery, $publishing_date, $author, $tags) = $this->get_details($data['url'][$counter], 1);
                                    $data['publishing_date'][$counter] = $publishing_date;
                                    $data['title'][$counter] = $title;

                                    $data['image_og'][$counter] = $image_og;
                                    $data['video'][$counter] = $video;
                                    $data['gallery'][$counter] = $gallery;
                                    $data['author'][$counter] = $author;
                                    $data['tags'][$counter] = $tags;
                                    foreach($tags as $tag){
                                        $tag_space = trim($tag);
                                         $tag_space = preg_replace("/[\s_]/", "_", $tag_space);
                                        $body_description =preg_replace('/\b'.$tag.'\b/u', '#'.$tag_space, $body_description);
                                        $description =preg_replace('/\b'.$tag.'\b/u', '#'.$tag_space, $description);
                                    }
                                    $data['body_description'][$counter] = $body_description;
                                    $data['desc'][$counter] = $description;
                                    $counter++;
                                }

                            }
                        }
                    }
                }
            }
        }
        echo "<pre>";
        print_r($data);
    }
    private function count_sports($category_id){
        $cdb = new CDbCriteria();
        $cdb->compare('category_id',11);

        return News::model()->count($cdb);
    }
    private function get_details($link,$id)
    {
        $html = new SimpleHTMLDOM();
        $html_url = $html->file_get_html($link);
        $page_details = PageDetails::model()->findAllByAttributes(array('source_id'=>$id));
        $author=null;
        $gallery = null;
        $publishing_date = null;
        $body_description = null;
        $description = null;
        $image = Yii::app()->params['domain']. 'image/general.jpg';;
        $video =null;
        $title=null;
        $tags=null;
        foreach($page_details as $detail) {

            //Author
            if (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'creator') {
                $author = $html_url->find($detail->predication, 0)->content;

            }
            //End author

            //title
            if (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'title') {
                $title = $html_url->find($detail->predication, 0)->plaintext;
            }
            //End title


            //Body desc
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'body_description') {
                $body_desc = $html_url->find($detail->predication);
                if (isset($body_desc)) {
                    foreach ($body_desc as $bodies) {
                        $body_description .= $bodies->plaintext;
                    }
                }
            }

            //End body desc

            //Description
            if (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'description') {

                $description = $html_url->find($detail->predication, 0)->content;
            }
            elseif (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'description') {
                $description = $html_url->find($detail->predication, 0)->plaintext;

            }
            //End Description

            //Publishing date
            if (isset($html_url->find($detail->predication, 0)->datetime) and !empty($html_url->find($detail->predication, 0)->datetime) and $detail->pageType->title == 'publishing_date') {
                $publishing_date = $html_url->find($detail->predication, 0)->datetime;
            }
            if (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'publishing_date') {
                $publishing_date = $html_url->find($detail->predication, 0)->plaintext;
            } elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'publishing_date') {
                $publishing_date = $html_url->find($detail->predication, 0)->content;
            }
            //End publishing date

            //All media
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'all_media') {
                $script = $html_url->find($detail->predication);
                if (!empty($script)) {
                    foreach ($script as $sc) {
                        $media = (array)json_decode($sc->innertext);
                    }
                    if (!empty($media)) {
                        if (isset($media['video'])) {
                            $video = $media['videos'];
                        }
                        if (isset($media['images'])) {
                            foreach ($media['images'] as $gall) {
                                $image = $media['images'][0];
                                if ($gall != $media['images'][0])
                                    $gallery[] = $gall;
                            }
                        }
                    }
                }
            }
            //End all media

            //image
            if (isset($html_url->find($detail->predication, 0)->src) and !empty($html_url->find($detail->predication, 0)->src) and $detail->pageType->title == 'image') {
                if (strpos($html_url->find($detail->predication, 0)->src, 'http') === 0) {
                    $image = $html_url->find($detail->predication, 0)->src;

                }else
                    $image = $html_url->find($detail->predication, 0)->src;
            } elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'image') {
                if (strpos($html_url->find($detail->predication, 0)->content, 'http') === 0)
                    $image = $html_url->find($detail->predication, 0)->content;
                else
                    $image = $html_url->find($detail->predication, 0)->content;

            }
            //end image

            //video
            if (isset($html_url->find($detail->predication, 0)->src) and !empty($html_url->find($detail->predication, 0)->src) and $detail->pageType->title == 'video') {
                $video = $html_url->find($detail->predication, 0)->src;
            } elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'video') {
                $video = $html_url->find($detail->predication, 0)->content;

            }
            //end video

            //gallery
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'gallery') {
                $gall = $html_url->find($detail->predication);
                foreach ($gall as $item) {
                    if (strpos($html_url->find($detail->predication, 0)->src, 'http') === 0) {
                        if($image != $item->src)
                            $gallery[] =$item->src;
                    }else {
                        if ($image !=  $item->src)
                            $gallery[] = $item->src;
                    }
                    }

            }
            //End gallery

            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'tags') {
                $tags_counter = $html_url->find($detail->predication);
                foreach ($tags_counter as $item) {
                    $tags[] =$item->plaintext;
                }

            }
        }
        return array($title,$description,$body_description,$video,$image,$gallery,$publishing_date,$author,$tags);
    }
}