<?php

/**
 * This is the model class for table "category_source".
 *
 * The followings are the available columns in table 'category_source':
 * @property integer $id
 * @property string $category
 * @property string $url
 * @property string $type
 * @property string $lang
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $deleted
 * @property integer $active
 * @property integer $updated_by
 *
 * The followings are the available model relations:
 * @property CategoryLevel[] $categoryLevels
 * @property PageDetails[] $pageDetails
 */
class CategorySource extends BaseModels
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'category_source';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category, url, type, created_at', 'required'),
			array('created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('category, url', 'length', 'max'=>255),
			array('type', 'length', 'max'=>3),
			array('category', 'check_exist'),
			array('updated_at,active,deleted,lang', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, category, url, type, created_at, updated_at, created_by, updated_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'categoryLevels' => array(self::HAS_MANY, 'CategoryLevel', 'source_id'),
			'pageDetails' => array(self::HAS_MANY, 'PageDetails', 'source_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category' => 'Category',
			'url' => 'Url',
			'type' => 'Type',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'created_by' => 'Created By',
			'updated_by' => 'Updated By',
		);
	}
    public function check_exist($att){
        $check_cat=CategorySource::model()->findByAttributes(array('category'=>trim($this->$att),'url'=>trim($this->url),'type'=>$this->type));

        if($check_cat && $check_cat->id!=$this->id){

            $this->addError('category', 'category is already exists');

        }

    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('category',$this->category,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('updated_by',$this->updated_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CategorySource the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function getrelat(){
        $cat = CategorySource::model()->findAll();
        $array = array();
        foreach ($cat as $index=>$items){
            if($items->type == 'rss'){
                array_push($array,$items);
            }elseif($items->type == 'dom'){
                if(isset($items->categoryLevels) && !empty($items->categoryLevels)){
                    array_push($array,$items);
                }
            }
        }
        return $array;
    }
}
