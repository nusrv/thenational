<?php
/* @var $this CategorySourceController */
/* @var $model CategorySource */

$this->breadcrumbs=array(
	'Category Sources'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CategorySource', 'url'=>array('index')),
	array('label'=>'Create CategorySource', 'url'=>array('create')),
	array('label'=>'View CategorySource', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CategorySource', 'url'=>array('admin')),
);
?>

<h1>Update CategorySource <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>