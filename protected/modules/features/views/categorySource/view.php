<?php
/* @var $this CategorySourceController */
/* @var $model CategorySource */

$this->breadcrumbs=array(
	'Category Sources'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CategorySource', 'url'=>array('index')),
	array('label'=>'Create CategorySource', 'url'=>array('create')),
	array('label'=>'Update CategorySource', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CategorySource', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CategorySource', 'url'=>array('admin')),
);
?>

<h1>View CategorySource #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'category',
		'url',
		'type',
		'created_at',
		'updated_at',
		'created_by',
		'updated_by',
	),
)); ?>
