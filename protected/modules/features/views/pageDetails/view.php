<?php
/* @var $this PageDetailsController */
/* @var $model PageDetails */

$this->breadcrumbs=array(
	'Page Details'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PageDetails', 'url'=>array('index')),
	array('label'=>'Create PageDetails', 'url'=>array('create')),
	array('label'=>'Update PageDetails', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PageDetails', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PageDetails', 'url'=>array('admin')),
);
?>

<h1>View PageDetails #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'source_id',
		'page_type_id',
		'predication',
		'created_at',
		'updated_at',
		'created_by',
		'updated_by',
	),
)); ?>
