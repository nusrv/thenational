<?php
/* @var $this PageDetailsController */
/* @var $model PageDetails */

$this->breadcrumbs=array(
	'Page Details'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PageDetails', 'url'=>array('index')),
	array('label'=>'Manage PageDetails', 'url'=>array('admin')),
);
?>

<h1>Create PageDetails</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>