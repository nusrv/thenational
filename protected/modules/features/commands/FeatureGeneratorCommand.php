<?php
class FeatureGeneratorCommand extends BaseCommand{

    // Please to use this command , run every 30 mint

    private $start_time;

    private $end_time;

    private $per_post_day;

    private $direct_push_start;

    private $direct_push_end;

    private $is_custom_settings;

    private $is_direct_push;

    private $scheduled_counter = 0;

    private $gap_time = 0;

    private $PlatFrom;

    private $rule;

    private $template;

    private $main_hash_tag;

    protected $hash_tag;

    protected $trim_hash;

    private $news_counter = 0;

    private $fb_is_scheduled = true;

    private $twitter_is_scheduled = true;

    private $instagram_is_scheduled = true;

    private $section_name = 'general';

    protected $category;

    protected $sub_category;

    private $settings;

    private $settings_category;

    private $all_dates = true;


    public function run($args)
    {
        $this->all_dates = true;

        if(isset($args[0]) && is_array($args)){
            if($args[0]=='ar'){
                $this->run_ar();
            }elseif($args[0]=='en'){
                $this->run_en();
            }
        }
    }

    public function run_ar(){


        Yii::import('application.modules.features.models.*');

        $this->TimeZone();

        $this->template = PostTemplate::model()->findAll('deleted = 0 ORDER BY RAND()');

        if (empty($this->template)) {
            echo 'Please insert template ' . PHP_EOL;
            exit();
        }

        system("clear");

        $this->rule = Yii::app()->params['rules'];

        $d = Hashtag::model()->findAll('deleted = 0');

        $this->trim_hash = array();

        $this->hash_tag = array();

        foreach ($d as $index => $item) {
            $this->trim_hash[$index] = " #" . str_replace(" ", "_", trim($item->title)) . ' ';
            $this->hash_tag[$index] = ' ' . trim($item->title) . ' ';
        }
        $this->main_hash_tag = Yii::app()->params['HashTagRoot'];

        $this->category = Category::model()->findAllByAttributes(array('deleted' => 0, 'active' => 1,'lang'=>'ar'));

        $this->settings = Settings::model()->findByPk(1);

        $this->PlatFrom = Platform::model()->findAllByAttributes(array('deleted' => 0));

        $this->per_post_day = $this->settings->how_many_posts;

        $this->start_time = $this->settings->start_date_time;

        $this->end_time = $this->settings->end_date_time;

        $this->gap_time = $this->settings->gap_time;

        $criteria = New CDbCriteria();

        $criteria->compare('is_scheduled', 1, true);

        $criteria->addCondition('parent_id IS NULL');

        $day = 0;

        $today = date('Y-m-d', strtotime(date('Y-m-d') . ' + ' . $day . ' day'));

        $start = date("Y-m-d H:i:s", strtotime($today . ' ' . $this->start_time));

        $end = date("Y-m-d H:i:s", strtotime($today . ' ' . $this->end_time));

        if ($end < $start)
            $criteria->addBetweenCondition('schedule_date', date('Y-m-d'), date('Y-m-d', strtotime(date('Y-m-d') . ' 1 day')), 'AND');
        else
            $criteria->compare('schedule_date', date('Y-m-d'), true);

        $this->scheduled_counter = PostQueue::model()->count($criteria);

        $this->settings_category = Settings::model()->findAll();

        /** add cron*/
        foreach($this->category as $cat) {

           if ($cat->type == 'dom')
                $this->Feed_dom_category();
            if($cat->type =='rss')
                $this->Feed_rss_category();

        }

    }

    public function run_en(){

        $this->TimeZone();

        $this->template = PostTemplate::model()->findAll('deleted = 0 ORDER BY RAND()');

        if (empty($this->template)) {
            echo 'Please insert template ' . PHP_EOL;
            exit();
        }

        system("clear");

        $this->rule = Yii::app()->params['rules'];

        $d = Hashtag::model()->findAll('deleted = 0');

        $this->trim_hash = array();

        $this->hash_tag = array();

        foreach ($d as $index => $item) {
            $this->trim_hash[$index] = " #" . str_replace(" ", "_", trim($item->title)) . ' ';
            $this->hash_tag[$index] = ' ' . trim($item->title) . ' ';
        }
        $this->main_hash_tag = Yii::app()->params['HashTagRoot'];

        $this->category = Category::model()->findAllByAttributes(array('deleted' => 0, 'active' => 1,'type'=>'en'));

        $this->sub_category = SubCategories::model()->findAllByAttributes(array('deleted' => 0, 'active' => 1,'type'=>'en'));

        $this->settings = Settings::model()->findByPk(1);

        $this->PlatFrom = Platform::model()->findAllByAttributes(array('deleted' => 0));

        $this->per_post_day = $this->settings->how_many_posts;

        $this->start_time = $this->settings->start_date_time;

        $this->end_time = $this->settings->end_date_time;

        $this->gap_time = $this->settings->gap_time;

        $criteria = New CDbCriteria();

        $criteria->compare('is_scheduled', 1, true);

        $criteria->addCondition('parent_id IS NULL');

        $day = 0;

        $today = date('Y-m-d', strtotime(date('Y-m-d') . ' + ' . $day . ' day'));

        $start = date("Y-m-d H:i:s", strtotime($today . ' ' . $this->start_time));

        $end = date("Y-m-d H:i:s", strtotime($today . ' ' . $this->end_time));

        if ($end < $start)
            $criteria->addBetweenCondition('schedule_date', date('Y-m-d'), date('Y-m-d', strtotime(date('Y-m-d') . ' 1 day')), 'AND');
        else
            $criteria->compare('schedule_date', date('Y-m-d'), true);

        $this->scheduled_counter = PostQueue::model()->count($criteria);

        $this->settings_category = Settings::model()->findAll();

        /** add cron*/

        foreach($this->category as $cat) {

            if ($cat->type == 'dom')
                $this->Feed_dom_category();
            if($cat->type =='rss')
                $this->Feed_rss_category();

        }

        //  $this->rssFeed_en_sub_category();
    }
    private function Feed_dom_category()
    {
        if (!is_array($this->category)) {
            echo '[Database] : sub category empty check please ' . PHP_EOL;
            exit();
        }

        $html = new SimpleHTMLDOM();
        if (!empty($this->category)) {
            foreach ($this->category as $sub) {
                if ($sub->type == 'dom'){
                if (!empty($sub->url)) {
                    $html_url = $html->file_get_html($sub->url);
                    if (isset($html_url)) {
                        $categories_predication = CategoryLevel::model()->findAllByAttributes(array('source_id' => $sub->id));
                        foreach ($categories_predication as $category_predict) {
                            $find = $category_predict->predication;
                            $list = $html_url->find($find);
                            if (isset($list)) {
                                foreach ($list as $item) {
                                    $url = null;
                                    $create = null;
                                    $date = null;
                                    $validateDate = false;
                                    if (isset($item->href) && !empty($item->href)) {
                                        if (strpos($item->href, 'http') === 0) {
                                            $url = $item->href;

                                        } else
                                            $url = Yii::app()->params['feedUrl'] . $item->href;


                                        $data['sub'] = $sub;

                                        $data['sub_category_id'] = null;

                                        $data['category_id'] = $sub->id;

                                        $data['creator'] = null;

                                        $data['column'] = null;

                                        $data['num_read'] = null;

                                        $data['title'] = null;

                                        $data['link'] = $url;

                                        $data['link_md5'] = md5($url);

                                        $data['description'] = null;

                                        $data['body_description'] = null;

                                        $data['shorten_url'] = null;

                                        $data['publishing_date'] = $date;

                                        if (!empty($data['link'])) {
                                            /*if (true) {*/
                                            list($title, $description, $body_description, $video, $image_og, $gallery, $publishing_date, $author) = $this->get_details($data['link'], $sub->id);
                                            $data['title'] = $this->clear_tags($title);
                                            $dates = str_replace("/", "-", $publishing_date);
                                            $date = date('Y-m-d', strtotime($dates));

                                            $data['publishing_date'] = $date;
                                            $data['description'] = $this->clear_tags($description);
                                            $data['body_description'] = $this->clear_tags($body_description);
                                            $data['creator'] = $author;
                                            $data['media']['video'] = $video;
                                            $data['media']['image'] = $image_og;

                                            $data['media']['gallery'] = $gallery;

                                            $this->settings_time($data['category_id']);

                                            $news = News::model()->findByAttributes(array('link_md5' => $data['link_md5']));
                                            if ($data['publishing_date'] == date('Y-m-d') or $this->all_dates) {

                                                if (empty($news)) {

                                                    $this->news_counter++;

                                                    $this->AddNews($data);

                                                } else
                                                    echo '[Database] : News is already exist ' . $news->id . PHP_EOL;
                                            } else
                                                echo '[Website] : This news is old date : ' . $data['publishing_date'] . ' Link is : ' . $url . PHP_EOL;
                                        } else {
                                            echo '[link] : Empty ' . $sub->url;

                                        }


                                    } else
                                        echo '[Website] : Url empty or does not exist in the current sub category id = ' . $sub->id . PHP_EOL;
                                }
                            }
                        }
                    } else
                        echo '[Sub link] : There is no data in this sub category ' . $sub->title . PHP_EOL;


                } else
                    echo '[Database] : Url empty in the current sub category id = ' . $sub->id . PHP_EOL;
            }
            }
        }
    }
    private function Feed_rss_category(){

        if (!is_array($this->category)) {
            echo '[Database] : sub category empty check please ' . PHP_EOL;
            exit();
        }

        if (!empty($this->category)) {

            foreach ($this->category as $sub) {
                if ($sub->type == 'rss'){
                    $rss = file_get_contents($sub->url);
                if (empty(trim($rss))) {
                    continue;
                }
                if (!empty($sub->url)) {
                    $xml = @simplexml_load_string($rss);
                    if (isset($xml)) {
                        foreach ($xml->channel->item as $item) {

                            if (isset($item)) {


                                $url = null;
                                $create = null;
                                $date = null;

                                $data['sub'] = $sub;

                                $data['sub_category_id'] = null;

                                $data['category_id'] = $sub->id;

                                $data['creator'] = null;

                                $data['column'] = null;

                                $data['num_read'] = null;

                                $data['title'] = null;

                                $data['link'] = $url;

                                $data['link_md5'] = md5($url);

                                $data['description'] = null;

                                $data['body_description'] = null;

                                $data['shorten_url'] = null;

                                $data['publishing_date'] = $date;

                                $namespaces = $item->getNameSpaces(true);
                                $dc = $item->children($namespaces['dc']);
                                $dc_date = $item->children($namespaces['dc']);

                                $e_content = $item->children("content", true);
                                if (isset($e_content->encoded))
                                    $e_encoded = (string)$e_content->encoded;


                                if (isset($dc))
                                    $data['creator'] = (string)$dc->creator;

                                if (isset($item->title))
                                    $data['title'] = strip_tags((string)$item->title);

                                if (isset($item->link)) {
                                    if (strpos($item->link, 'http') === 0) {
                                        $url = (string)$item->link;

                                    } else
                                        $url = Yii::app()->params['feedUrl'] . (string)$item->link;

                                    $data['link'] = $url;
                                    $data['link_md5'] = $url;
                                }
                                if (isset($item->description))
                                    $data['description'] = strip_tags((string)$item->description);

                                if (isset($e_encoded))
                                    $data['body_description'] = strip_tags($e_encoded);

                                if (isset($item->pubDate))
                                    $pub_date = (string)$item->pubDate;
                                elseif (isset($dc_date))
                                    $pub_date = $dc_date->date;

                                if (isset($pub_date))
                                    $data['publishing_date'] = date('Y-m-d H:i:s', strtotime(trim($pub_date)));

                                if (isset($url) && !empty($url)) {


                                    if (!empty($data['link'])) {
                                        /*if (true) {*/
                                        list($title, $description, $body_description, $video, $image_og, $gallery, $publishing_date, $author) = $this->get_details($data['link'], $sub->id);
                                        if (!empty($title))
                                            $data['title'] = $this->clear_tags($title);
                                        if (!empty($publishing_date)) {
                                            $dates = str_replace("/", "-", $publishing_date);
                                            $date = date('Y-m-d', strtotime($dates));

                                            $data['publishing_date'] = $date;
                                        }
                                        if (!empty($description))
                                            $data['description'] = $this->clear_tags($description);
                                        if (!empty($body_description))
                                            $data['body_description'] = $this->clear_tags($body_description);
                                        if (!empty($author))
                                            $data['creator'] = $author;
                                        $data['media']['video'] = $video;
                                        $data['media']['image'] = $image_og;

                                        $data['media']['gallery'] = $gallery;

                                        $this->settings_time($data['category_id']);

                                        $news = News::model()->findByAttributes(array('link_md5' => $data['link_md5']));
                                        if ($data['publishing_date'] == date('Y-m-d') or $this->all_dates) {

                                            if (empty($news)) {

                                                $this->news_counter++;

                                                $this->AddNews($data);

                                            } else
                                                echo '[Database] : News is already exist ' . $news->id . PHP_EOL;
                                        } else
                                            echo '[Website] : This news is old date : ' . $data['publishing_date'] . ' Link is : ' . $url . PHP_EOL;
                                    } else {
                                        echo '[link] : Empty ' . $sub->url;

                                    }


                                } else
                                    echo '[Website] : Url empty or does not exist in the current sub category id = ' . $sub->id . PHP_EOL;

                            }
                        }
                    } else
                        echo '[Sub link] : There is no data in this sub category ' . $sub->title . PHP_EOL;


                } else
                    echo '[Database] : Url empty in the current sub category id = ' . $sub->id . PHP_EOL;
            }
            }
        }
    }
    private function get_details($link,$id)
    {
        $html = new SimpleHTMLDOM();
        $html_url = $html->file_get_html($link);
        $page_details = PageDetails::model()->findAllByAttributes(array('source_id'=>$id));
        $author=null;
        $gallery = null;
        $publishing_date = null;
        $body_description = null;
        $description = null;
        $image = Yii::app()->params['domain']. 'image/general.jpg';;
        $video =null;
        $title=null;
        foreach($page_details as $detail) {

            //Author
            if (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'creator') {
                $author = $html_url->find($detail->predication, 0)->content;

            }
            elseif (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'creator') {
                $author = $html_url->find($detail->predication, 0)->plaintext;
            }
            //End author

            //title
            if (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'title') {
                $title = $html_url->find($detail->predication, 0)->plaintext;
            }
            elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'title') {
                $title = $html_url->find($detail->predication, 0)->content;

            }
            //End title


            //Body desc
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'body_description') {
                $body_desc = $html_url->find($detail->predication);
                if (isset($body_desc)) {
                    foreach ($body_desc as $bodies) {
                        $body_description .= $bodies->plaintext;
                    }
                }
            }elseif (!empty($html_url->find($detail->predication,0)->plaintext) and isset($html_url->find($detail->predication,0)->plaintext) and $detail->pageType->title == 'body_description') {
                $body_description = $html_url->find($detail->predication,0)->plaintext;
            }


                //End body desc

            //Description
            if (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'description') {

                $description = $html_url->find($detail->predication, 0)->content;
            }
            elseif (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'description') {
                $description = $html_url->find($detail->predication, 0)->plaintext;

            }
            //End Description

            //Publishing date
            if (isset($html_url->find($detail->predication, 0)->datetime) and !empty($html_url->find($detail->predication, 0)->datetime) and $detail->pageType->title == 'publishing_date') {
                $publishing_date = $html_url->find($detail->predication, 0)->datetime;
            }
            if (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'publishing_date') {
                $publishing_date = $html_url->find($detail->predication, 0)->plaintext;
            } elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'publishing_date') {
                $publishing_date = $html_url->find($detail->predication, 0)->content;
            }
            //End publishing date

            //All media
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'all_media') {
                $script = $html_url->find($detail->predication);
                if (!empty($script)) {
                    foreach ($script as $sc) {
                        $media = (array)json_decode($sc->innertext);
                    }
                    if (!empty($media)) {
                        if (isset($media['videos'])) {
                            $video = $media['videos'][0];
                        }
                        if (isset($media['images'])) {
                            foreach ($media['images'] as $gall) {
                                $image = 'http://www.wam.ae' . $media['images'][0];
                                if ($gall != $media['images'][0])
                                    $gallery[] = 'http://www.wam.ae' . $gall;
                            }
                        }
                    }
                }
            }
            //End all media

            //image
            if (isset($html_url->find($detail->predication, 0)->src) and !empty($html_url->find($detail->predication, 0)->src) and $detail->pageType->title == 'image') {
                if (strpos($html_url->find($detail->predication, 0)->src, 'http') === 0) {
                    $image = $html_url->find($detail->predication, 0)->src;

                }else
                    $image = 'http://www.emaratalyoum.com'.$html_url->find($detail->predication, 0)->src;
            } elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'image') {
                if (strpos($html_url->find($detail->predication, 0)->content, 'http') === 0)
                    $image = $html_url->find($detail->predication, 0)->content;
                else
                    $image = 'http://www.emaratalyoum.com'.$html_url->find($detail->predication, 0)->content;

            }
            //end image

            //video
            if (isset($html_url->find($detail->predication, 0)->src) and !empty($html_url->find($detail->predication, 0)->src) and $detail->pageType->title == 'video') {
                $video = $html_url->find($detail->predication, 0)->src;
            } elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'video') {
                $video = $html_url->find($detail->predication, 0)->content;

            }else if (isset($html_url->find($detail->predication, 0)->data) and !empty($html_url->find($detail->predication, 0)->data) and $detail->pageType->title == 'video') {
                $video = $html_url->find($detail->predication, 0)->data;
            }
            //end video

            //gallery
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'gallery') {
                $gall = $html_url->find($detail->predication);
                foreach ($gall as $item) {
                    if (strpos($html_url->find($detail->predication, 0)->src, 'http') === 0) {
                        if($image != $item->src)
                            $gallery[] =$item->src;
                    }else {
                        if ($image != 'http://www.emaratalyoum.com' . $item->src)
                            $gallery[] = 'http://www.emaratalyoum.com' .$item->src;
                    }
                }

            }
            //End gallery
        }

        return array($title,$description,$body_description,$video,$image,$gallery,$publishing_date,$author);
    }

    private function settings_time($category_id)
    {

        $catch = false;
        if ($this->section_name != 'general') {
            $category_id = 1;
        }

        foreach ($this->settings_category as $item) {
            if ($item->category_id == $category_id) {
                $settings_category = $item;
                $catch = true;
                break;
            }
        }

        if (!empty($settings_category) && $catch) {
            $this->start_time = $settings_category->start_date_time;
            $this->end_time = $settings_category->end_date_time;
            $this->gap_time = $settings_category->gap_time;
            $this->direct_push_start = $settings_category->direct_push_start;
            $this->direct_push_end = $settings_category->direct_push_end;
            $this->is_custom_settings = true;
            $this->is_direct_push = $settings_category->direct_push;
        } else {
            $this->start_time = $this->settings->start_date_time;
            $this->end_time = $this->settings->end_date_time;
            $this->gap_time = $this->settings->gap_time;
            $this->direct_push_start = $this->settings->direct_push_start;
            $this->direct_push_end = $this->settings->direct_push_end;
            $this->is_custom_settings = false;
            $this->is_direct_push = $this->settings->direct_push;
        }
        $type = 'general';
        if ($catch)
            $type = 'custom';

        return $type;
    }

    private function date($cat_id)
    {
        $final_date = null;
        $condition = new CDbCriteria();
        $condition->order = 'id Desc';
        $condition->limit = 1;

        if ($this->section_name != 'general') {
            $cat_id = 1;
        }

        if ($this->is_custom_settings) {
            $condition->condition = 'is_scheduled = 1 and settings="custom" and main_category_id = ' . $cat_id;
        } else {
            $condition->condition = 'is_scheduled = 1 and settings="general"';
        }

        $check = PostQueue::model()->find($condition);
        $is_scheduled = 1;
        $now = strtotime(date('Y-m-d H:i:s') . ' + ' . $this->gap_time . ' minutes');
        $start = strtotime(date('Y-m-d') . ' ' . $this->start_time);
        $end = strtotime(date('Y-m-d') . ' ' . $this->end_time);


        if ($end < $start) {
            $extra_day = 1;
            $end = strtotime(date('Y-m-d') . ' ' . $this->end_time . ' + ' . $extra_day . ' day');
        }

        if (empty($check)) {
            if (($now <= $start)) {
                $final_date = $start;

            } elseif ($now <= $end) {
                $final_date = $now;
            } else {
                $final_date = $now;
                $is_scheduled = 0;
            }
        } else {
            $date = strtotime($check->schedule_date . ' + ' . $this->gap_time . ' minutes');
            if ($date < $now) {
                $date = $now;
            }

            if (($date <= $start)) {
                $final_date = $start;
            } elseif ($date <= $end) {
                $final_date = $date;
            } else {
                $final_date = $date;
                $is_scheduled = 0;
            }
        }

        if ($this->is_direct_push) {
            $direct_start = strtotime(date('Y-m-d') . ' ' . $this->direct_push_start);
            $direct_end = strtotime(date('Y-m-d') . ' ' . $this->direct_push_end);

            if ($direct_end < $direct_start) {
                $extra_day = 1;
                $direct_end = strtotime(date('Y-m-d') . ' ' . $this->direct_push_end . ' + ' . $extra_day . ' day');
            }
            $now = strtotime(date('Y-m-d H:i:s'));
            if (($now >= $direct_start) && ($now <= $direct_end)) {
                $final_date = strtotime(date('Y-m-d H:i:s') . ' -1 minute');
                $is_scheduled = 1;
            }
        }

        $post_date = date('Y-m-d H:i:s', $final_date);

        return array($post_date, $is_scheduled);

    }

    private function AddNews($data)
    {
        echo '[Generators] : new news on database : md5 link [ '.$data['link_md5'].' ]'.PHP_EOL;
        $news = new News();

        $news->id = null;

        $news->link = $data['link'];

        $news->link_md5 = $data['link_md5'];

        $news->category_id = $data['category_id'];

        $news->sub_category_id = $data['sub_category_id'];

        $news->title = $data['title'];

        $news->column = $data['column'];

        $news->description = $data['description'];

        $news->body_description = $data['body_description'];

        $news->publishing_date = $data['publishing_date'];

        $news->schedule_date = $data['publishing_date'];

        $news->created_at = date('Y-m-d H:i:s');

        $news->creator = $data['creator'];

        $shortUrl = new ShortUrl();

        $news->shorten_url = $shortUrl->short(urldecode($news->link));

        list($post_date, $is_scheduled) = $this->date($data['category_id']);

        $news->schedule_date = $post_date;

        $news->setIsNewRecord(true);

        if ($news->save(true)) {

            $id = null;

            if (isset($data['media']['video']))
                $id = $this->AddMediaNews($news->id, 'video', $data['media']['video']);

            if (isset($data['media']['image']))
                $id = $this->AddMediaNews($news->id, 'image', $data['media']['image']);

            if (isset($data['media']['gallery']))
                $id = $this->AddMediaNews($news->id, 'gallery', $data['media']['gallery']);

            $this->newsGenerator($news, isset($data['media']['gallery']) ? 'gallery' : 'image',$id, $is_scheduled);

        } else {
            echo "News Already Exist in the database" . PHP_EOL;
        }

    }

    public function AddMediaNews($news_id, $type, $data)
    {

        $id = null;
        if ($type == 'gallery') {
            foreach ($data as $item) {
                if (empty(MediaNews::model()->findByAttributes(array('media' => $item, 'news_id' => $news_id)))) {
                    $media = new MediaNews();
                    $media->type = $type;
                    $media->media = $item;
                    $media->news_id = $news_id;
                    $media->setIsNewRecord(true);
                    $media->save(false);
                    if ($id == null)
                        $id = $media->id;
                }
            }
            return $id;
        }

        if (empty(MediaNews::model()->findByAttributes(array('media' => $data, 'news_id' => $news_id)))) {
            $media = new MediaNews();
            $media->type = $type;
            $media->media = $data;
            $media->news_id = $news_id;
            $media->setIsNewRecord(true);
            $media->save(false);
            if ($id == null)
                $id = $media->id;
        }

        return $id;


    }

    protected function newsGenerator($news, $type,$id, $is_scheduled)
    {

        $this->scheduled_counter++;

        $parent = null;

        if ($this->scheduled_counter > $this->per_post_day) {
            $is_scheduled = 0;
        }

        $category = '#' . trim(str_replace(array('!', '"', ':', '.', '..', '...', ' '), '_', $news->category->title));

        $sub_category = '';
        if(isset($news->subCategory))
            $sub_category = '#' . trim(str_replace(array('!', '"', ':', '.', '..', '...', ' '), '_', $news->subCategory->title));


        foreach ($this->PlatFrom as $item) {

            $twitter_is_scheduled = false;

            $media = null;

            $temp = $this->get_template($item, $news->category_id, $type);

            $title = $news->title;

            $description = $news->description;

            if ($item->title != 'Instagram') {
                $title = str_replace($this->hash_tag, $this->trim_hash, $news->title);
                $description = str_replace($this->hash_tag, $this->trim_hash, $news->description);
            }

            $title = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $title);

            $title = preg_replace('/\n(\s*\n){2,}/', "\n\n", $title);

            $description = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $description);

            $description = preg_replace('/\n(\s*\n){2,}/', "\n\n", $description);

            $shorten_url = $news->shorten_url;

            $full_creator = $news->creator;

            $text = str_replace(
                array('[title]', '[description]', '[short_link]', '[author]'),
                array($title, $description, $shorten_url, $full_creator),
                $temp['text']
            );

            if ($item->title == 'Facebook' or $item->title == 'Twitter') {

                $text = str_replace('# ', '#', $text);

                $found = true;

                preg_match_all("/#([^\s]+)/", $text, $matches);

                if (isset($matches[0])) {
                    $matches[0] = array_reverse($matches[0]);
                    $count = 0;
                    foreach ($matches[0] as $hashtag) {
                        if ($count >= 2) {
                            $found = false;
                            $hashtag_without = str_replace('#', '', $hashtag);
                            $hashtag_without = str_replace('_', ' ', $hashtag_without);
                            $text = str_replace($hashtag, $hashtag_without, $text);
                        }
                        $count++;
                    }
                    if ($count >= 2)
                        $found = false;
                }

                if ($found)
                    $text = str_replace(array('[section]', '[sub_section]'), array($category, $sub_category,), $text);
                else
                    $text = str_replace(array('[section]', '[sub_section]', '|'), array('', '', '',), $text);

            } elseif ($item->title == 'Instagram') {
                $text = str_replace('# ', '#', $text);
                $text = str_replace(array('[section]', '[sub_section]', '|'), array('', '', ''), $text);
            }


            $newsMedia = MediaNews::model()->findByPk($id);

            if(!empty($newsMedia))
                $media = $newsMedia->media;

            if ($item->title == 'Twitter') {
                $text_twitter = $text;
                if ($temp['type'] == 'Preview')
                    if (!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!', $text, $matches))
                        $text = $text . PHP_EOL . $news->shorten_url;

                if ($this->getTweetLength($text_twitter, $temp['type'] == 'Image' ? true : false, $temp['type'] == 'Video' ? true : false) > 141)
                    $is_scheduled = 0;
                else {
                    $text = $text_twitter . PHP_EOL;
                    if ($temp['type'] == 'Preview')
                        if (!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!', $text, $matches))
                            $text .= PHP_EOL . $news->shorten_url;
                }
            } else
                if ($twitter_is_scheduled)
                    if (!$is_scheduled)
                        $is_scheduled = 1;

            $text = str_replace('# #', '#', $text);
            $text = str_replace('##', '#', $text);
            $text = str_replace('&#8220;', '', $text);
            $text = str_replace('&#8221;', '', $text);
            $text = str_replace('&#8230;', '', $text);
            $text = str_replace('&#x2014;', '', $text);
            $text = str_replace('#8211;', '', $text);
            $text = str_replace('&#8211;', '', $text);
            $text = str_replace('&nbsp;', '', $text);
            $text = str_replace('&#160;', '', $text);
            $text = str_replace('&#8230;', '', $text);
            $text = str_replace('#8217;t', '', $text);
            $text = str_replace('#8217;', '', $text);
            $text = str_replace('. .', '', $text);
            $text = str_replace('&#xf2;,', '', $text);
            $text = str_replace('&#8211;', '', $text);
            $text = str_replace('&ndash;', '', $text);
            if ($item->title != 'Instagram') {
                $text = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $text);
                $text = preg_replace('/\n(\s*\n){2,}/', "\n\n", $text);
            }


            if ($item->title == 'Twitter') {
                if (!$this->twitter_is_scheduled)
                    $is_scheduled = 0;
            }


            if ($item->title == 'Instagram') {
                if (!$this->instagram_is_scheduled)
                    $is_scheduled = 0;
            }


            $PostQueue = new PostQueue();
            $PostQueue->setIsNewRecord(true);
            $PostQueue->id = null;
            $PostQueue->command = false;
            $PostQueue->type = $temp['type'];
            $PostQueue->post = trim($text);
            $PostQueue->schedule_date = $news->schedule_date;
            $PostQueue->catgory_id = $news->category_id;
            $PostQueue->main_category_id = $this->section_name != 'general' ? 1 : $news->category_id;
            $PostQueue->link = $news->shorten_url;
            $PostQueue->is_posted = 0;
            $PostQueue->news_id = $news->id;
            $PostQueue->post_id = null;
            $PostQueue->media_url = $media;
            $PostQueue->settings = $this->is_custom_settings ? 'custom' : 'general';
            $PostQueue->is_scheduled = $is_scheduled;
            $PostQueue->platform_id = $item->id;
            $PostQueue->generated = 'auto';
            $PostQueue->created_at = date('Y-m-d H:i:s');
            if ($parent == null) {
                $PostQueue->parent_id = null;
                if ($PostQueue->save())
                    $parent = $PostQueue->id;
                else
                    $this->send_email('Error on GeneratorsCommand.php ', 'error on post queue save  <a href="' . Yii::app()->params['domain'] . '/postQueue/' . $PostQueue->id . '"></a>');

            } else {
                $PostQueue->parent_id = $parent;
                if (!$PostQueue->save())
                    $this->send_email('Error on GeneratorsCommand.php ', 'error on post queue save  <a href="' . Yii::app()->params['domain'] . '/postQueue/' . $PostQueue->id . '"></a>');
            }

        }

        $news->generated = 1;
        if (!$news->save())
            $this->send_email('Error on GeneratorsCommand.php ', 'error on post queue save  <a href="' . Yii::app()->params['domain'] . '/news/' . $news->id . '"></a>');


    }

    private function get_template($platform, $category, $type)
    {

        $temp = PostTemplate::model()->findByAttributes(array(
            'platform_id' => $platform->id,
            'catgory_id' => $category,
        ));
        if (!empty($temp))
            return $temp;

        $cond = new CDbCriteria();
        $cond->order = 'RAND()';
        $cond->condition = '( ( platform_id = ' . $platform->id . ' or platform_id is NUll ) and  catgory_id is NULL )';
        $temp = PostTemplate::model()->find($cond);

        if (!empty($temp))
            return $temp;

        if ($platform->title == 'Instagram') {
            isset($temp->id) ? $temp->id : null;
        }

        return Yii::app()->params['templates'];
    }




}