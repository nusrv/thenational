<?php

/**
 * This is the model class for table "time_settings".
 *
 * The followings are the available columns in table 'time_settings':
 * @property integer $id
 * @property integer $platform_category_settings_id
 * @property string $start_time
 * @property string $end_time
 * @property integer $is_direct_push
 * @property string $direct_push_start_time
 * @property string $direct_push_end_time
 * @property integer $gap_time
 * @property integer $obj_id
 * @property string $updated_at
 * @property string $created_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class TimeSettings extends SettingsBaseModels
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'time_settings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('platform_category_settings_id, start_time, end_time, gap_time', 'required'),
			array('platform_category_settings_id,obj_id, is_direct_push, gap_time, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, platform_category_settings_id, start_time, end_time, is_direct_push, direct_push_start_time, direct_push_end_time, gap_time, updated_at, created_at, created_by, updated_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'platform_category_settings_id' => 'Platform Category Settings',
			'start_time' => 'Start Time',
			'end_time' => 'End Time',
			'is_direct_push' => 'Is Direct Push',
			'direct_push_start_time' => 'Direct Push Start Time',
			'direct_push_end_time' => 'Direct Push End Time',
			'gap_time' => 'Gap Time',
			'obj_id' => 'obj_id',
			'updated_at' => 'Updated At',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
			'updated_by' => 'Updated By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('platform_category_settings_id',$this->platform_category_settings_id);
		$criteria->compare('start_time',$this->start_time,true);
		$criteria->compare('end_time',$this->end_time,true);
		$criteria->compare('is_direct_push',$this->is_direct_push);
		$criteria->compare('direct_push_start_time',$this->direct_push_start_time,true);
		$criteria->compare('direct_push_end_time',$this->direct_push_end_time,true);
		$criteria->compare('gap_time',$this->gap_time);
		$criteria->compare('obj_id',$this->obj_id);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('updated_by',$this->updated_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TimeSettings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
