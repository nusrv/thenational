<?php
/* @var $form TbActiveForm */
$form=$this->beginWidget('booster.widgets.TbActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'id'=>'form-visible',
)); ?>
    <div class="col-sm-2">
        <?php echo $form->textFieldGroup(
            $model,
            'id',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_id',
                    array(
                        'inline'=>true,
                        //'labelOptions'=>array('class'=>'btn btn-primary'),
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                                // 'style'=>'opacity: 0;'
                            ),
                        ),
                    )
                ),
                'widgetOptions'=>array(

                    'htmlOptions'=>array(

                        'disabled'=>$model->visible_id?false:true
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>
    <div class="col-sm-2">
        <?php echo $form->dropDownListGroup(
            $model,
            'name',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_name',
                    array(
                        'inline'=>true,
                        //'labelOptions'=>array('class'=>'btn btn-primary'),
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                               // 'style'=>'opacity: 0;'
                            ),
                        ),
                    )
                ),
                'widgetOptions'=>array(
                    'data'=>array_merge(array(''=>'Choose one'),CHtml::listData(User::model()->findAll('deleted=0'),'name','name')),
                    'htmlOptions'=>array(

                        'disabled'=>$model->visible_name?false:true
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>
    <div class="col-sm-2">
        <?php echo $form->dropDownListGroup(
            $model,
            'username',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_username',
                    array(
                        'inline'=>true,
                        //'labelOptions'=>array('class'=>'btn btn-primary'),
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                                // 'style'=>'opacity: 0;'
                            ),
                        ),
                    )
                ),
                'widgetOptions'=>array(
                    'data'=>array_merge(array(''=>'Choose one'),CHtml::listData(User::model()->findAll('deleted=0'),'username','username')),
                    'htmlOptions'=>array(

                        'disabled'=>$model->visible_username?false:true
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>

    <div class="col-sm-2">
        <?php echo $form->dropDownListGroup(
            $model,
            'active',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_active',
                    array(
                        'inline'=>true,
                         'widgetOptions'=>array(
                            'htmlOptions'=>array(
                             ),
                        ),
                    )
                ),
                'widgetOptions'=>array(
                    'data'=>array_merge(array(''=>'All status'),array('0'=>'Disabled','1'=>'Active')),
                    'htmlOptions'=>array(
                        'disabled'=>$model->visible_active?false:true
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>
    <div class="col-sm-2">
        <?php echo $form->dropDownListGroup(
            $model,
            'gender',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_gender',
                    array(
                        'inline'=>true,
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                            ),
                        ),
                    )
                ),
                'widgetOptions'=>array(
                    'data'=>array_merge(array(''=>'All genders'),array('f'=>'Female','m'=>'Male')),
                    'htmlOptions'=>array(
                        'disabled'=>$model->visible_gender?false:true
                    )
                ),

            )
        ); ?>

    </div>


    <div class="col-sm-2">
        <?php echo $form->datePickerGroup(
            $model,
            'created_at',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_created_at',
                    array(
                        'inline'=>true,
                         'widgetOptions'=>array(
                            'htmlOptions'=>array(
                             ),
                        ),
                    )
                ),
                'widgetOptions'=>array(
                    'options' => array(
                        'language' => 'en',
                        'format' => 'yyyy-mm-dd',
                        'viewformat' => 'yyyy-mm-dd',
                    ),

                    'htmlOptions'=>array(
                        'disabled'=>$model->visible_created_at?false:true
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>
    <div class="col-sm-3 col-sm-push-4">
        <?php echo $form->checkboxGroup(
            $model,
            'visible_profile_pic',
            array(
                'inline'=>true,
                'widgetOptions'=>array(

                ),
            )
        );



        ?>

    </div><div class="col-sm-2 col-sm-push-3">
        <?php echo $form->checkboxGroup(
            $model,
            'visible_background_color',
            array(
                'inline'=>true,
                'widgetOptions'=>array(

                ),
            )
        );



        ?>

    </div>

    <div class="col-sm-2 pull-left page-sizes"  >

        <?php echo $form->dropDownListGroup(
            $model,
            'pagination_size',
            array(

                'widgetOptions'=>array(
                    'data'=>$model->pages_size(),
                    'htmlOptions'=>array(

                    ),
                ),
                'hint'=>''
            )
        ); ?>
    </div>



<?php $this->endWidget(); ?>