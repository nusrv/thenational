<?php
/* @var $this DefaultController */
/* @var $model PostQueue */
/* @var $form TbActiveForm */
$form = $this->beginWidget('booster.widgets.TbActiveForm',
    array(
        'id' => 'search-form',
        'type' => 'horizontal',
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    )
);
?>
<style>
    span.required {
        display: none;
    }

</style>
<br />
 <div class="row">


    <div class="col-sm-6">


        <?PHP
        echo $form->datePickerGroup(
            $model,
            'schedule_date',
            array(
                'widgetOptions' => array(
                    'options' => array(
                        'viewformat' => 'yyyy-mm-dd',
                        'format' => 'yyyy-mm-dd',
                    ),
                ),
                'labelOptions' => array(
                    'class' => 'col-sm-3'
                ),
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-9 col-sm-pull-1',
                    'style' => 'padding-left: 14px;'
                ),
            )
        );


        ?></div>
    <div class="col-sm-6">

        <?PHP
        echo $form->datePickerGroup(
            $model,
            'to',
            array(
                'widgetOptions' => array(
                    'options' => array(
                        'viewformat' => 'yyyy-mm-dd',
                        'format' => 'yyyy-mm-dd',
                    )
                ),
                'labelOptions' => array(
                    'class' => 'col-sm-3'
                ),
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-9 col-sm-pull-1',
                    'style' => 'padding-left: 14px;'
                ),
            )
        );
        ?></div>

</div>
<?PHP $this->endWidget(); ?>
