<?php
class TPHomeController extends ThematicPostBaseControllers
{
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('index','previewPost','getTwitterSize'),
                'users'=>array(Yii::app()->user->getState('type')),
                array('deny',  // deny all users
                    'users'=>array('*'),
                ),
            ),

        );
    }


    public $gallery_data =null;

    public function news($url){

        $link = Yii::app()->params['feedUrl'];
        $category_ids = null;
        $sub_category_ids = null;
        if(!isset($url) && empty($url))
            exit();
        $valid_link =true;

        $check_link = strpos($url,$link) ==0;


        if($check_link){
            $this->category = explode($link,$url);

            if(is_array($this->category) && isset($this->category[1]) && !empty($this->category[1])){

                $split = explode('/',$this->category[1]);

                if(is_array($split) && isset($split[1]) && !empty($split[1])){
                    $this->category = array();
                    $sub_section_item = $split[1];
                    if($split[1] == 'blogs'){
                        $sub_section_item ='uae';
                    }
                    if(!empty($split[2])) {

                        $cdb = new CDbCriteria();
                        $cdb->condition = '`url` like "' . trim(Yii::app()->params['feedUrl'] . '/' . $sub_section_item.'/'.$split[2]) . '%"';
                        $cdb->order = 'id DESC';

                        $this->category = Category::model()->find($cdb);
                        $this->section_selected = $split[2];
                    }
                    if(empty($this->category)) {

                        $cdb = new CDbCriteria();
                        $cdb->condition = '`url` like "' . trim(Yii::app()->params['feedUrl'] . '/' . $sub_section_item) . '%"';
                        $cdb->order = 'id DESC';

                        $this->category = Category::model()->find($cdb);
                        $this->section_selected = $split[1];
                    }

                    if(empty($this->category)){

                        $this->category = Category::model()->findByPk(62);
                        $this->section_selected = $split[1];
                        if(strlen($split[1]) >= 25){
                            $this->section_selected ='';
                        }


                    }


                }
                elseif(empty($split[1])){

                    $valid_link=false;
                }
                else
                    $this->category = null;
                if(is_array($split) && isset($split[3]) && !empty($split[3])){

                    $this->sub_category = SubCategories::model()->findByAttributes(array('url' => trim(Yii::app()->params['feedUrl'].'/'.$split[1].'/'.$split[2])));

                }
                elseif(empty($split[2])){

                    $valid_link=false;
                }
                else
                    $this->sub_category = null;
            }elseif(empty(empty($this->category[1]))){
                $valid_link=false;
            }else{
                $this->sub_category = null;
                $this->category = null;
            }


            $info = array();
            if(isset($this->category->id) and !empty($this->category->id)){
                $valid_link=true;
                $this->category_id = $this->category->id;
                $info['category_id'] = $this->category_id;
                $category_ids = $info['category_id'];
            }else{
                $valid_link = false;
            }
            if(isset($this->sub_category->id) and !empty($this->sub_category->id)){

                $this->sub_category_id = $this->sub_category->id;
                $info['sub_category_id'] = $this->sub_category_id;
                $sub_category_ids = $info['sub_category_id'];
            }
            if(!$valid_link){
                $this->model->addError('link','please insert valid link');
                return false;
            }

            $data_info = $this->get_info_features($url,$category_ids,$sub_category_ids);
            if($data_info=='error_html'){
                $this->model->addError('link','Something went wrong please try again');
                return false;
            }
            if(is_array($data_info)){

                $info = array_merge($info,$data_info);

                $this->shorten_url =$this->bitShort(urldecode($info['link']));
                $info['shorten_url'] = $this->shorten_url;
                $info['link_md5'] = md5(urldecode($info['link']));

                return $this->generator_info($info);

            }


            return false;


        }

        return false;

    }


    public function actionIndex()
    {

        $this->breadcrumbs=array('Thematic');

        $this->setPageTitle(Yii::app()->name.' - Thematic');

        $this->model=new GeneratorPost();

        $this->performAjaxValidation();
        if(isset($_POST['GeneratorPost']['link']) && !empty($_POST['GeneratorPost']['link']))

            if (isset($_POST['post_now_btn']) or isset($_POST['schedule_btn']))

                $_POST['GeneratorPost']['submits'] = 1;


        if(isset($_POST['GeneratorPost']['submits']) && $_POST['GeneratorPost']['submits'] == 0 ){

            if(isset($_POST['GeneratorPost']['link']) && !empty($_POST['GeneratorPost']['link']))
            {
                $this->preview_date();
                $this->model->link = $_POST['GeneratorPost']['link'];
            }

            else
                $this->model->addError('link','Link is not valid');

        }else{


            if(isset($_POST['GeneratorPost']))
            {

                $this->model->attributes  =$_POST['GeneratorPost'];

                $data = $_POST['GeneratorPost'];

                if(!empty($data['link'])){

                    if(isset($_POST['post_now_btn'])){

                        $data['time'] = date('Y-m-d H:i:s');




                        $this->add_new_data_post_now($data);


                    }elseif(isset($_POST['schedule_btn'])){

                        $this->add_new_data($data);

                    }

                }
                else
                    $this->model->addError('link','Link cannot be blank.');
            }
        }

        $this->render('index',array(
            'model'=>$this->model,
        ));
    }


    public function preview_date(){

        $data = $this->news(trim($_POST['GeneratorPost']['link']));


        if($data == false){
            $this->model->addError('link','Please check the link you used.');
            return false;
        }


        $this->model->category_id = isset($data['info']['category_id'])?$data['info']['category_id']:null;
        $this->model->schedule = 1;
        $this->model->new_category = isset($this->new_category)?$this->new_category:null;

        $this->model->time = $_POST['GeneratorPost']['time'];

        foreach ($data as $index => $item) {

            if($index != 'info'){

                foreach ($item as $index_items =>$items) :

                    if($index_items !='platform'){
                        $val = $index.'_'.$index_items;
                        $this->model->$val = $items;
                    }else
                        $this->model->platforms[]= $items;


                endforeach;

            }else{

                foreach ($item as $index_info => $item_info):

                    if($index_info == 'image')
                        $this->model->image = $item_info['src'];
                    elseif($index_info == 'gallary') {
                        $this->model->gallary = $item_info['src'];
                    }
                    else
                        $this->model->$index_info = $item_info;

                endforeach;

            }
        }
    }

    public function add_new_data($data){


        if(empty(Category::model()->findByPk($data['category_id'])))
            $data['category_id'] = 1 ;


        if(isset($data['platforms']) and !empty($data['platforms'])) {
            foreach ($data['platforms'] as $item) {
                $byAttribute = Platform::model()->findByPk($item);

                if(empty(trim($data[strtolower($byAttribute->title).'_post']))){
                    foreach ($data as $index => $values) {
                        $this->model->$index = $values;
                    }
                    $this->model->submits =0;
                    $this->model->addError(strtolower($byAttribute->title).'_post', strtolower($byAttribute->title).' post cannot be blank');
                    return false;
                }
                $text = null;
                $type = null;
                if ($byAttribute->title == 'Twitter') {
                    if ($this->getTweetLength($data['twitter_post'], $data['twitter_type'] == 'Image' ? true : false, $data['twitter_type'] == 'Video' ? true : false) > 141) {
                        foreach ($data as $index => $values) {
                            $this->model->$index = $values;
                        }
                        $this->model->addError('twitter_post', 'twitter size must be less than 140 chars long');
                        return false;
                    }
                }
            }
        }else{
            foreach ($data as $index => $values) {
                $this->model->$index = $values;
            }
            $this->model->submits =0;
            $this->model->addError('platforms', 'Platforms cannot be blank');
            return false;

        }
        $news = new News();
        $news->id = null;
        $news->title = isset($data['title'])?trim($data['title']):null;
        $news->link = isset($data['link'])?trim($data['link']):null;
        $news->description = isset($data['description'])?trim($data['description']):null;
        $news->category_id = isset($data['category_id'])?$data['category_id']:null;
        $news->sub_category_id = isset($data['sub_category_id'])?$data['sub_category_id']:null;
        $news->creator = isset($data['creator'])?trim($data['creator']):null;
        $news->column = isset($data['column'])?trim($data['column']):$news->creator;
        $news->link_md5 = isset($data['link_md5'])?$data['link_md5']:null;
        $news->shorten_url =isset( $data['shorten_url'])? trim($data['shorten_url']):null;
        $news->publishing_date = empty($data['time'])?date('Y-m-d H:i:s'):$data['time'];
        $news->schedule_date =  empty($data['time'])?date('Y-m-d H:i:s'):$data['time'];
        $news->generated = 0;
        $news->setIsNewRecord(true);
        $news->created_at = date('Y-m-d H:i:s');

        $image = isset($data['image'])?$data['image']:null;

        /*if(empty($image)){
            if(isset($info['info']['gallary']['src'])){
                $image = key($info['info']['gallary']['src']);
            }
        }*/

        if($news->save(false)){

            $media = new MediaNews();
            $media->id = null;
            $media->media = $image;
            $media->news_id = $news->id ;
            $media->type ='image';
            $media->setIsNewRecord(true);
            $media->save(false);
            if(isset($data['gallary'])){
                foreach($data['gallary'] as $gallery){
                    if($image != $gallery) {
                        $media = new MediaNews();
                        $media->id = null;
                        $media->media = $gallery;
                        $media->news_id = $news->id;
                        $media->type ='gallery';
                        $media->setIsNewRecord(true);
                        $media->save(false);
                    }
                }
            }
        }


        if(isset($data['platforms']) and !empty($data['platforms'])){
            foreach ($data['platforms'] as $item) {
                $byAttribute = Platform::model()->findByPk($item);
                $text = null;
                $type = null;
                if($byAttribute->title == 'Facebook') {
                    $text = $data['facebook_post'];
                    $type = $data['facebook_type'];
                }

                if($byAttribute->title == 'Twitter') {
                    $type = $data['twitter_type'];
                    $text = $data['twitter_post'];

                }

                if($byAttribute->title == 'Instagram') {
                    $text = $data['instagram_post'];
                    $type = $data['instagram_type'];

                }


                $this->PostQueue = new PostQueue();
                $this->PostQueue->id= null;
                $this->PostQueue->setIsNewRecord(true);
                $this->PostQueue->post = trim($text);
                $this->PostQueue->type = trim($type);
                $this->PostQueue->schedule_date = empty($news->schedule_date)?date('Y-m-d H:i:s'):$news->schedule_date;
                $this->PostQueue->catgory_id =  $news->category_id;
                $this->PostQueue->main_category_id =  null;
                $this->PostQueue->link = trim($news->shorten_url);
                $this->PostQueue->is_posted = 0;
                $this->PostQueue->news_id = $news->id;
                $this->PostQueue->post_id =null;
                $this->PostQueue->media_url =$image;
                $this->PostQueue->settings ='custom';
                $this->PostQueue->is_scheduled =1;
                $this->PostQueue->approved=1;
                $this->PostQueue->platform_id =$byAttribute->id;
                $this->PostQueue->generated ='thematic';
                $this->PostQueue->pinned =$data['pinned'];
                $this->PostQueue->created_at =date('Y-m-d H:i:s');

                if($this->parent == null){
                    $this->PostQueue->parent_id =null;
                    if($this->PostQueue->save())
                        $this->parent = $this->PostQueue->id;

                }else{
                    $this->PostQueue->parent_id =$this->parent;
                    $this->PostQueue->save();
                }
            }
        }else{
            foreach ($data as $index => $values) {
                $this->model->$index = $values;
            }
            $this->model->submits =0;
            $this->model->addError('platforms', 'Platforms cannot be blank');
            return false;

        }

        $news->generated = 1;
        $news->save(false);
        if(!empty($this->parent))
            $this->redirect(array('/postQueue/main'));
    }


    public function add_new_data_post_now($data){


        /*if(!empty($data['link'])) {
            $info = $this->news_thematic($data['link']);*/
        $info=array();

        $array_platforms = array(1=>'facebook',2=>'twitter',3=>'instagram');
        if(!empty($data['link'])) {
            if(!empty($data['twitter_type'])){
                foreach ($data as $index=>$val){

                    $index = explode('_',$index);

                    if(array_search($index[0],$array_platforms)){
                        if(!empty($index[1])){
                            $info[$index[0]][$index[1]]=$val;
                        }else{
                            $info[$index[0]]=$val;
                        }
                    }else{

                        if(!empty($index[2])){
                            $info['info'][$index[0].'_'.$index[1].'_'.$index[2]]=$val;
                        }
                        elseif(!empty($index[1])){
                            $info['info'][$index[0].'_'.$index[1]]=$val;
                        }elseif(!empty($index[0])){

                            $info['info'][$index[0]]=$val;
                        }
                    }
                }
            }else{
                $info = $this->news($data['link']);

            }





            if(empty($info)){
                $this->model->submits =0;
                $this->model->addError('link', 'Please insert a valid link');
                return false;
            }

            if (isset($_POST['post_now_btn']))
                $data['time'] = date('Y-m-d H:i:s');



            if(isset($data['platforms']) and !empty($data['platforms'])) {
                foreach ($data['platforms'] as $item) {
                    $byAttribute = Platform::model()->findByPk($item);
                    $text = null;
                    $type = null;
                    if (empty($data['twitter_type'])) {
                        if ($byAttribute->title == 'Twitter') {
                            if ($this->getTweetLength($info['twitter']['post'], $info['twitter']['type'] == 'Image' ? true : false, $info['twitter']['type'] == 'Video' ? true : false) > 141) {
                                $this->preview_date();
                                $this->model->submits =0;
                                $this->model->addError('twitter_post', 'twitter size must be less than 140 chars long');
                                return false;
                            }
                        }
                    } else {
                        if(empty(trim($data[strtolower($byAttribute->title).'_post']))){
                            foreach ($data as $index => $values) {
                                $this->model->$index = $values;
                            }
                            $this->model->submits =0;
                            $this->model->addError(strtolower($byAttribute->title).'_post', strtolower($byAttribute->title).' post cannot be blank');
                            return false;
                        }
                        if ($byAttribute->title == 'Twitter') {
                            if ($this->getTweetLength($data['twitter_post'], $data['twitter_type'] == 'Image' ? true : false, $data['twitter_type'] == 'Video' ? true : false) > 141) {
                                foreach ($data as $index => $values) {
                                    $this->model->$index = $values;
                                }
                                $this->model->submits =0;
                                $this->model->addError('twitter_post', 'twitter size must be less than 140 chars long');
                                return false;
                            }
                        }
                    }
                }
            }else{
                $this->model->addError('platforms', 'Platforms cannot be blank');
                $this->model->submits =0;
                return false;

            }

            if(empty(Category::model()->findByPk($info['info']['category_id']))) {
                if (empty(Category::model()->findByPk($data['category_id']))) {
                    $data['category_id'] = 1;
                    $info['info']['category_id'] = 1;
                }
            }

            $news = new News();
            $news->id = null;
            $news->title = isset($info['info']['title']) ? trim($info['info']['title']) : null;
            $news->link = isset($info['info']['link']) ? trim($info['info']['link']) : null;
            $news->description = isset($info['info']['description']) ? trim($info['info']['description']) : null;
            $news->category_id = isset($info['info']['category_id']) ? $info['info']['category_id'] : null;
            $news->sub_category_id = isset($info['info']['sub_category_id']) ? $info['info']['sub_category_id'] : null;
            $news->creator = isset($info['info']['creator']) ? trim($info['info']['creator']) : null;
            $news->column = isset($info['info']['column']) ? trim($info['info']['column']) : $news->creator;
            $news->link_md5 = isset($info['info']['link_md5']) ? trim($info['info']['link_md5']) : null;
            $news->shorten_url = isset($info['info']['shorten_url']) ? trim($info['info']['shorten_url']) : null;
            $news->publishing_date = empty($data['time']) ? date('Y-m-d H:i:s') : $data['time'];
            $news->schedule_date = empty($data['time']) ? date('Y-m-d H:i:s') : $data['time'];
            $news->generated = 0;
            $news->setIsNewRecord(true);
            $news->created_at = date('Y-m-d H:i:s');

            $image = isset($info['info']['image']['src']) ? $info['info']['image']['src'] : null;

            if (empty($image)) {
                if (isset($info['info']['gallary']['src'])) {
                    $image = key($info['info']['gallary']['src']);
                }
            }
            if(isset($data['image']) and !empty($data['image'])){
                $image=$data['image'];
            }

            if($news->save(false)){
                $media = new MediaNews();
                $media->id = null;
                $media->media = $image;
                $media->news_id = $news->id ;
                $media->type ='image';
                $media->setIsNewRecord(true);
                $media->save(false);
                if(isset($data['gallary'])){
                    foreach($data['gallary'] as $gallery){
                        if($image != $gallery) {
                            $media = new MediaNews();
                            $media->id = null;
                            $media->media = $gallery;
                            $media->news_id = $news->id;
                            $media->type ='gallery';
                            $media->setIsNewRecord(true);
                            $media->save(false);
                        }
                    }
                }
            }

            if (isset($data['platforms']) and !empty($data['platforms'])){

                foreach ($data['platforms'] as $item) {
                    $byAttribute = Platform::model()->findByPk($item);
                    $text = null;
                    $type = null;
                    if ($byAttribute->title == 'Facebook') {
                        if(!empty($data['facebook_post'])){
                            $text = $data['facebook_post'];
                        }else{
                            $text = $info['facebook']['post'];
                        }
                        $type = $info['facebook']['type'];
                    }
                    if ($byAttribute->title == 'Twitter') {
                        if(!empty($data['twitter_post'])){
                            $text = $data['twitter_post'];
                        }else{
                            $text = $info['twitter']['post'];
                        }
                        $type = $info['twitter']['type'];

                    }
                    if ($byAttribute->title == 'Instagram') {
                        if(!empty($data['instagram_post'])){
                            $text = $data['instagram_post'];
                        }else{
                            $text = $info['instagram']['post'];
                        }
                        $type = $info['instagram']['type'];
                    }

                    $this->PostQueue = new PostQueue();
                    $this->PostQueue->id = null;
                    $this->PostQueue->setIsNewRecord(true);
                    $this->PostQueue->post = trim($text);
                    $this->PostQueue->type = trim($type);
                    $this->PostQueue->schedule_date = empty($news->schedule_date) ? date('Y-m-d H:i:s') : $news->schedule_date;
                    $this->PostQueue->catgory_id = $news->category_id;
                    $this->PostQueue->main_category_id = null;
                    $this->PostQueue->link = trim($news->shorten_url);
                    $this->PostQueue->is_posted = 0;
                    $this->PostQueue->news_id = $news->id;
                    $this->PostQueue->post_id = null;
                    $this->PostQueue->media_url = $image;
                    $this->PostQueue->settings = 'custom';
                    $this->PostQueue->approved=1;
                    $this->PostQueue->is_scheduled = 1;
                    $this->PostQueue->platform_id = $byAttribute->id;
                    $this->PostQueue->generated = 'thematic';
                    $this->PostQueue->pinned = $data['pinned'];
                    $this->PostQueue->created_at = date('Y-m-d H:i:s');

                    if ($this->parent == null) {
                        $this->PostQueue->parent_id = null;
                        if ($this->PostQueue->save())
                            $this->parent = $this->PostQueue->id;

                    } else {
                        $this->PostQueue->parent_id = $this->parent;
                        $this->PostQueue->save();
                    }
                }
            }else{
                foreach ($data as $index => $values) {
                    $this->model->$index = $values;
                }
                $this->model->submits =0;
                $this->model->addError('platforms', 'Platforms cannot be blank');
                return false;

            }


            $news->generated = 1;
            $news->save(false);
            if (!empty($this->parent))
                $this->redirect(array('/postQueue/main'));
        }

        else
            $this->model->addError('link','Link cannot be blank.');
    }
    public function actionGetTwitterSize(){
        if(isset($_GET)) {
            echo $this->getTweetLength($_GET['text'], $_GET['type'] == 'Image' ? true : false, $_GET['type'] == 'Video' ? true : false);
        }
    }

    protected function performAjaxValidation()
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='generator-post-form')
        {
            echo CActiveForm::validate($this->model);
            Yii::app()->end();
        }
    }








}